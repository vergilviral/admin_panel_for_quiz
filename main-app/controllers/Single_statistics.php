<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Single_statistics extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Sub_statistics_mod');
    }
    public function index()
    {
        $name = $this->session->user_name;
        $avail_subs = $this->Sub_statistics_mod->find_all_subjects();

        if(isset($avail_subs['error'])){
            $this->session->set_flashdata('error', $avail_subs['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $data = array(
                'name' => $name,
                'avail_subjects' => $avail_subs,
            );

            $message = $this->session->flashdata('message');

            $error = $this->session->flashdata('error');

            $msg = $this->session->flashdata('msg');

            if(isset($message)){
                $data['message'] = $message;
            }
            if(isset($error)){
                $data['error'] = $error;
            }
            if(isset($msg)){
                $data['msg'] = $msg;
            }


            $this->load->view('static/header');
            $this->load->view('pages/all_subjects', $data);
            $this->load->view('static/footer');
        }
    }

    public function show_stats()
    {
        $this->form_validation->set_rules('sub_names_avail','Available Subjects', 'required', array('required' => 'You have not provided Subject Name.'));
        if($this->form_validation->run() == TRUE)
        {
            $sub_info = $this->Sub_statistics_mod->get_stats();
            if(isset($sub_info['error']))
            {
                $this->session->set_flashdata('error', $sub_info['error']);
                header("refresh:1,url=". base_url() ."home");
            }

            else
            {
                $data2 = array(
                    'sub_info' => $sub_info,
                );

                $this->load->view('static/header_single_sub_stats');
                $this->load->view('pages/single_stats', $data2);
                $this->load->view('static/footer');
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again.');
            header("refresh:1,url=". base_url() ."home");
        }
    }

}
