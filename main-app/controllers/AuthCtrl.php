<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthCtrl extends CI_Controller {
	public function __construct()
	{
		$this->details = array();
		parent::__construct();
		$this->load->model('AuthMod');
// 		$this->load->helper( 'url');
// 		$this->load->library('session');
	}
	public function index()
	{
        $message = $this->session->flashdata('message');
        $msg = $this->session->flashdata('msg');
        $error = $this->session->flashdata('error');
        $data = array();
        if(isset($message)) {
            $data['message'] = $message;
        }
        if(isset($msg)) {
            $data['msg'] = $msg;
//            echo $data['msg'];

        }
        if(isset($error)){
            $data['error'] = $error;
        }
        $this->load->view('static/headerforlogin', $data);
		$this->load->view('Auth/login', $data);
//		$this->load->view('static/footer');

	}

	public function login_verify()
	{
		$user_name = $this->input->post('username');
		$this->form_validation->set_rules('username', 'Username' , 'required' );
		$this->form_validation->set_rules('password', 'Password' , 'required' );
		if($this->form_validation->run() == TRUE)
		{

			$results = $this->AuthMod->login();
			if(isset($results['error']))
			{
                $this->session->set_flashdata('error', $results['error']);
                redirect(base_url() .'AuthCtrl', 'refresh');
			}

			else
			{
				$this->session->set_flashdata('msg', 'Welcome '. $user_name);
				// redirect('home', 'refresh');
				header("refresh:1,url=". base_url() ."home");

			}
		}
		else
		{
            $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again. ');
            header("refresh:1,url=". base_url() ."AuthCtrl");
		}
	}

	public function register(){
        $data = array (
            'error' => 'Sorry no registration allowed right now. You can sign up for beta at vedaquiz.com/signup.'
        );
        $this->load->view('static/headerforlogin', $data);
        $this->load->view('Auth/login', $data);

//		$this->load->view('static/headerforlogin');
//		$this->load->view('Auth/register');
//		$this->load->view('static/footer');
	}


	public function registerthisuser(){

		$this->load->view('static/headerforlogin');
		$this->load->view('Auth/register');
	}

	public function new_user()
	{
		$this->form_validation->set_rules('username', 'Username' , 'required|alpha_numeric|trim' );
		$this->form_validation->set_rules('first_name', 'First Name' , 'required' );
		$this->form_validation->set_rules('last_name', 'Last Name' , 'required' );
		$this->form_validation->set_rules('email', 'Email' , 'required|valid_email' );
		$this->form_validation->set_rules('password', 'Password' , 'required' );
		$this->form_validation->set_rules('confirm_pass', 'Confirm Password' , 'required|matches[password]' );

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->AuthMod->add_new_user();
			if(isset($data['error'])) {
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=". base_url() ."AuthCtrl");
			}
			else {
				$this->session->set_flashdata("msg", "Registration Successful. Please activate your account from link we sent you in your email. Do not forget to check spam/junk folder ;)");
				redirect(base_url() .'AuthCtrl', 'refresh');
			}
		}
		else
		{
            $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again.');
            header("refresh:1,url=". base_url() ."AuthCtrl");
		}
	}

    public function verify($verificationText=NULL){
        $noRecords = $this->AuthMod->verifyEmailAddress($verificationText);

        if ($noRecords > 0) {
            $this->session->set_flashdata('msg', "Email verified successfully. You can login now to continue.");
            redirect(base_url() .'AuthCtrl', 'refresh');
        }

        else {
            $this->session->set_flashdata('error', "Email verification failed. Please try again. If you are facing any difficulty, do not hesitate to reach us.");
            redirect(base_url() .'AuthCtrl', 'refresh');
        }
    }


    public function logout()
	{
		$data = array('user_name','logged_in', 'email', 'u_name', 'profile_pic', 'quiz_id', 'img_path');
		$this->session->unset_userdata($data);
		redirect(base_url() .'', 'refresh');
	}
}
