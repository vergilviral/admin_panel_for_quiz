<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Forgot_password_mod');
        $this->load->model('Email_notification_mod');
    }

    public function index()
    {
        $message = $this->session->flashdata('message');

        $error = $this->session->flashdata('error');

        $msg = $this->session->flashdata('msg');

        $data = array();

        if(isset($message)){
            $data['message'] = $message;
        }
        if(isset($error)){
            $data['error'] = $error;
        }
        if(isset($msg)){
            $data['msg'] = $msg;
        }

        $this->load->view('static/headerforlogin' ,$data);
        $this->load->view('Auth/forgot_password', $data);
//        $this->load->view('static/footer');

    }

    public function verify_details()
    {
        $user_name = $this->input->post('username');
        $this->form_validation->set_rules('username', 'Username' , 'required' );

        if($this->form_validation->run() == TRUE) {

            $results = $this->Forgot_password_mod->send_details();

            if (isset($results['error'])) {
                $this->session->set_flashdata('error', $results['error']);
                header("refresh:1,url=" . base_url() . "Forgot_password");
            }

            else {
                $this->Email_notification_mod->forgot_password($results['random_pass'],$results['email']);
            $this->session->set_flashdata('msg', $results['msg']);
            redirect(base_url() . 'AuthCtrl', 'refresh');
            }
        }

        else {
            $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again. ');
            header("refresh:1,url=". base_url() ."Forgot_password");
        }
    }
}
