<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Feedback_mod');
        $this->load->model('Email_notification_mod');

    }
    public function index()
    {
        $name = $this->session->user_name;
//        $avail_quiz = $this->Get_links_mod->find_all_quiz();

//        if(isset($avail_quiz['error'])){
//            $this->session->set_flashdata('error', $avail_quiz['error']);
//            header("refresh:1,url=". base_url() ."Get_Links");
//        }
//        else {
        $data = array(
            'name' => $name,
//            'avail_quizzes' => $avail_quiz,
        );

        $message = $this->session->flashdata('message');

        $error = $this->session->flashdata('error');

        $msg = $this->session->flashdata('msg');

        if(isset($message)){
            $data['message'] = $message;
        }
        if(isset($error)){
            $data['error'] = $error;
        }
        if(isset($msg)){
            $data['msg'] = $msg;
        }

        $this->load->view('static/header');
        $this->load->view('pages/feedback', $data);
        $this->load->view('static/footer');
//        }
    }

    public function submit_feedback(){
        $feedback = $this->Feedback_mod->submit_feedback();

        if(isset($feedback['error']))
        {
            $this->session->set_flashdata('error', $feedback['error']);
            redirect(base_url() .'AuthCtrl', 'refresh');
        }

        else
        {
            $this->Email_notification_mod->new_feedback($feedback);
            $this->session->set_flashdata('msg', 'Feedback Submitted Successfully. Thank you.');
            // redirect('home', 'refresh');
            header("refresh:1,url=". base_url() ."home");

        }
    }

}
