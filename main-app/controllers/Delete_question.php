<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete_question extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Edit_questions_mod');
        $this->load->model('Delete_question_mod');
    }
    public function index()
    {
        $name = $this->session->user_name;
        $avail_quiz = $this->Edit_questions_mod->find_all_quiz();

        $message = $this->session->flashdata('message');

        $error = $this->session->flashdata('error');

        $msg = $this->session->flashdata('msg');

        if(isset($avail_quiz['error'])){
            $this->session->set_flashdata('error', $avail_quiz['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $data = array(
                'name' => $name,
                'avail_quizzes' => $avail_quiz,
            );

            if(isset($message)){
                $data['message'] = $message;
            }
            if(isset($error)){
                $data['error'] = $error;
            }
            if(isset($msg)){
                $data['msg'] = $msg;
            }


            $this->load->view('static/header');
            $this->load->view('pages/delete_question_list_quiz', $data);
            $this->load->view('static/footer');
        }
    }

    public function list_questions(){
        $this->form_validation->set_rules('quiz_names_avail', 'Available Quizzes' , 'required' );

        if($this->form_validation->run() == TRUE)
        {
            $data = $this->Edit_questions_mod->list_questions();

            if(isset($data['error']))
            {
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=". base_url() ."Delete_question");
            }
            else{

                $data2 = array(
                    'questions_list' => $data,
                );

                $this->load->view('static/header');
                $this->load->view('pages/delete_question_list_question', $data2);
                $this->load->view('static/footer');
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Oops. Something Went Wrong. Please Try Again.');
            header("refresh:1,url=". base_url() ."Delete_question");
        }

    }

    public function delete()
    {
        $this->form_validation->set_rules('selected_question', 'Selected Question' , 'required' );

        if($this->form_validation->run() == TRUE)
        {
            $data = $this->Delete_question_mod->delete();

            if(isset($data['error']))
            {
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=". base_url() ."Delete_question");
            }

            else {
                $this->session->set_flashdata('msg', $data['msg']);
                header("refresh:1,url=". base_url() ."/home");
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Oops. Something Went Wrong. Please Try Again.');
            header("refresh:1,url=". base_url() ."Delete_question");
        }
    }

}
