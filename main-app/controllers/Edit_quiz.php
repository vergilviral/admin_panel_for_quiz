<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_quiz extends CI_Controller
{
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Edit_quiz_mod');
    }

    public function index()
    {
        $name = $this->session->user_name;
        $avail_quiz = $this->Edit_quiz_mod->find_all_quiz();
        if (isset($avail_quiz['error'])) {
            $this->session->set_flashdata('error', $avail_quiz['error']);
            header("refresh:1,url=" . base_url() . "home");
        } else {
            $data = array(
                'name' => $name,
                'avail_quizzes' => $avail_quiz,
            );

            $message = $this->session->flashdata('message');

            $error = $this->session->flashdata('error');

            $msg = $this->session->flashdata('msg');

            if (isset($message)) {
                $data['message'] = $message;
            }
            if (isset($error)) {
                $data['error'] = $error;
            }
            if (isset($msg)) {
                $data['msg'] = $msg;
            }

            $this->load->view('static/header');
            $this->load->view('pages/edit_quiz_all_quiz', $data);
            $this->load->view('static/footer');
        }
    }


    public function edit_single_quiz()
    {
        $data = $this->Edit_quiz_mod->get_single_quiz_details();

//        echo $data['quiz_name'] . 'anyyyyything';

        if (isset($data['error'])) {
            $this->session->set_flashdata('error', $data['error']);
            redirect(base_url() . "Edit_quiz", 'refresh');
        } else {
            $data2 = array(
                'single_quiz_details' => $data,
            );

            $this->load->view('static/header');
            $this->load->view('pages/edit_single_quiz', $data2);
            $this->load->view('static/footer');
        }
    }


    public function update_single_quiz()
    {
        $this->form_validation->set_rules('quiz', 'Quiz Name', 'required');
        $this->form_validation->set_rules('sub', 'Subject Name', 'required');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->Edit_quiz_mod->update_single_quiz();

            if (isset($data['error'])) {
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=" . base_url() . "Edit_quiz");
            } else {
                $this->session->set_flashdata('msg', $data['message']);
                header("refresh:1,url=" . base_url() . "Edit_quiz");
            }
        } else {
            $this->session->set_flashdata('error', 'Something Went Wrong. Please Try Again.');
            header("refresh:1,url=" . base_url() . "Edit_quiz");
        }
    }
}