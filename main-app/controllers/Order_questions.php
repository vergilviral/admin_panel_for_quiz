<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_questions extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Order_questions_mod');
    }
    public function index()
    {
        $name = $this->session->user_name;
        $avail_quiz = $this->Order_questions_mod->find_all_quiz();
        if(isset($avail_quiz['error'])){
            $this->session->set_flashdata('error', $avail_quiz['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $data = array(
                'name' => $name,
                'avail_quizzes' => $avail_quiz,
            );

            $message = $this->session->flashdata('message');

            $error = $this->session->flashdata('error');

            $msg = $this->session->flashdata('msg');

            if(isset($message)){
                $data['message'] = $message;
            }
            if(isset($error)){
                $data['error'] = $error;
            }
            if(isset($msg)){
                $data['msg'] = $msg;
            }

            $this->load->view('static/header');
            $this->load->view('pages/order_select_quiz', $data);
            $this->load->view('static/footer');
        }
    }

    public function list_questions(){
        $this->form_validation->set_rules('quiz_names_avail', 'Available Quizzes' , 'required' );

        if($this->form_validation->run() == TRUE) {

            $data = $this->Order_questions_mod->list_questions();

            if(isset($data['error'])) {
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=". base_url() ."Edit_questions");
            }
            else
            {
                $data2 = array(
                    'questions_list' => $data,
                );

                $this->load->view('static/header');
                $this->load->view('pages/order_list_questions', $data2);
                $this->load->view('static/footer');
            }
        }
        else {
            $this->session->set_flashdata('error', 'Oops. Something Went Wrong. Please Try Again.');
            header("refresh:1,url=". base_url() ."Edit_questions");
        }

    }

    public function update_order(){
        $data = $this->Order_questions_mod->update_order();

        echo $data['message'];

        if(isset($data['error'])) {
            $this->session->set_flashdata('error', $data['error']);
//                header("refresh:1,url=". base_url() ."Edit_questions");
            redirect(base_url() ."Order_questions", 'refresh');
        }

        else {
            $this->session->set_flashdata('message', 'Order updated successfully.');
            redirect(base_url() ."Order_questions", 'refresh');

//            header("refresh:1,url=". base_url() ."Order_questions");
        }
    }

}
