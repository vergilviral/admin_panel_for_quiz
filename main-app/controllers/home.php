<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        
        $this->details = array();
        parent::__construct();
        $this->load->model('home_mod');
        $this->load->model('Edit_profile_mod');
    }

    public function index()
    {
        if(isset($this->session->user_name)){
            $this->Edit_profile_mod->get_single_user_data();
    
            $profile_pic = $this->session->profile_pic;
    
            $quiz_info = $this->home_mod->get_quiz_info();
    
            $message = $this->session->flashdata('message');
    
            $msg = $this->session->flashdata('msg');
    
            $error = $this->session->flashdata('error');
    
            if (isset($quiz_info['error'])) {
                $quiz_info = '';
                $data = array(
                    'error' => $quiz_info['error'],
                    'quiz_info' => $quiz_info,
                    'profile_pic' => $profile_pic,
                );
                $this->load->view('static/header_home', $data);
                $this->load->view('pages/dashboard', $data);
                $this->load->view('static/footer');
    
            }
    
            if (!isset($quiz_info['error'])) {
    
                if (isset($user)) {
                    $data = array(
                        'msg' => $message,
                        'u_name' => $user,
                        'quiz_info' => $quiz_info,
                        'profile_pic' => $profile_pic,
                    );
                    $this->load->view('static/header_home', $data);
                    $this->load->view('pages/dashboard', $data);
                    $this->load->view('static/footer');
    
                } else {
                    $data = array(
                        'quiz_info' => $quiz_info,
                        'profile_pic' => $profile_pic,
                    );
    
                    if(isset($message)){
                        $data['message'] = $message;
                    }
                    if(isset($error)){
                        $data['error'] = $error;
                    }
                    if(isset($msg)){
                        $data['msg'] = $msg;
                    }
                    $this->load->view('static/header_home', $data);
                    $this->load->view('pages/dashboard', $data);
                    $this->load->view('static/footer');
                }
            }
        }
        else{
            $this->session->set_flashdata('error', 'Please login first.');
            header("refresh:1,url=". base_url() ."");

        }
    }
}
