<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_single_quiz_results extends CI_Controller {
    public function __construct()
    {
        $this->details = array();
        parent::__construct();
        $this->load->model('Get_single_quiz_results_mod');
        $this->load->helper( 'url');
        $this->load->library('session');
    }

    public function index()
    {
        $name = $this->session->user_name;
        $avail_quiz = $this->Get_single_quiz_results_mod->find_all_quiz();

        if(isset($avail_quiz['error'])){
            $this->session->set_flashdata('error', $avail_quiz['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $data = array(
                'name' => $name,
                'avail_quizzes' => $avail_quiz,
            );

            $message = $this->session->flashdata('message');

            $error = $this->session->flashdata('error');

            $msg = $this->session->flashdata('msg');

            if(isset($message)){
                $data['message'] = $message;
            }
            if(isset($error)){
                $data['error'] = $error;
            }
            if(isset($msg)){
                $data['msg'] = $msg;
            }

            $this->load->view('static/header');
            $this->load->view('pages/get_single_results', $data);
            $this->load->view('static/footer');
        }
    }

    public function get_result(){
        $quiz_name = $this->Get_single_quiz_results_mod->get_quiz_name();

        if(isset($quiz_name['error'])){
            $this->session->set_flashdata('error', $quiz_name['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $trimmed_quiz_name = preg_replace('/\s+/', '', $quiz_name);
            $filename = '' . $trimmed_quiz_name . '_Final_Results_' . date('Ymd') . '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");

            $usersData = $this->Get_single_quiz_results_mod->get_single_result();

            if (!isset($usersData['error'])) {
                $file = fopen('php://output', 'w');

                $header = array("Username", "Score");

                fputcsv($file, $header);
                foreach ($usersData as $key => $line) {
                    fputcsv($file, $line);
                }
                fclose($file);
                exit;
            }

            else {
                $this->session->set_flashdata('error', 'Oops. Something Went Wrong. Please Try Again.');
                header("refresh:1,url=". base_url() ."home");
            }
        }
    }
}
