<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_profile extends CI_Controller {
	public function __construct()
	{
		$this->details = array();
		parent::__construct();
		$this->load->model('Edit_profile_mod');
        $this->load->model('Email_notification_mod');
	}
	public function index()
	{
	    if(isset($this->session->user_name)){
    		$name = $this->session->user_name;
    		$user_details = $this->Edit_profile_mod->get_single_user_data();
    
    		if(isset($user_details['error'])){
                $this->session->set_flashdata('error', $user_details['error']);
                header("refresh:1,url=". base_url() ."AuthCtrl");
            }
    
            else {
                $data = array(
                    'name' => $name,
                    'user_details' => $user_details[0],
                );
                $message = $this->session->flashdata('message');
    
                $error = $this->session->flashdata('error');
    
                $msg = $this->session->flashdata('msg');
    
                if(isset($message)){
                    $data['message'] = $message;
                }
                if(isset($error)){
                    $data['error'] = $error;
                }
                if(isset($msg)){
                    $data['msg'] = $msg;
                }
    
                if(isset($user_details['message'])){
                    $data['message'] = $user_details['message'];
                }
    
    //            echo $data['message'] . 'x';
                $this->load->view('static/header');
                $this->load->view('pages/user_profile', $data);
                $this->load->view('static/footer');
            }
	    }
	    else{
            $this->session->set_flashdata('error', 'Please login first.');
            header("refresh:1,url=". base_url() ."");

        }

	}

	public function edit_single_profile(){
        $settings =  array(
            'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/",
            'upload_url'      => base_url()."uploads/",
            'allowed_types'   => "gif|jpg|png|jpeg|pdf|doc|xml",
            'overwrite'       => TRUE,
            'max_size'        => "2000KB",
            'quality'         => '60%',
            'encrypt_name' => TRUE,
        );

        $this->load->library('upload', $settings);

        if($this->upload->do_upload()) {
            $file_name = $this->upload->data('file_name');
        }
        else {
            $file_name = '';
        }

		$this->form_validation->set_rules('pass', 'Password', 'trim');
		$this->form_validation->set_rules('confirm_pass', 'Password Confirmation', 'trim|matches[pass]');

		if($this->form_validation->run() == TRUE) {
			$data = $this->Edit_profile_mod->update_profile($file_name);

			if(isset($data['error']))
			{
                $this->session->set_flashdata('error', $data['error']);
                header("refresh:1,url=". base_url() ."Edit_profile");
			}

			if(isset($data['message'])){
			    if(isset($data['pass_changed']) == 1){
			        if($data['pass_changed'] == 1) {
                        $this->Email_notification_mod->password_changed();
                    }
                }
                if(isset($data['email_changed']) == 1){
                    if($data['email_changed'] == 1) {
                        $this->Email_notification_mod->email_changed();
                    }
                }

                $name = $this->session->user_name;
				$user_details = $this->Edit_profile_mod->get_single_user_data();
				$data2 = array(
					'name' => $name,
					'user_details' => $user_details[0],
					'msg' => $data['message'],
				);

                $this->session->set_flashdata('msg', $data['message']);
                header("refresh:1,url=". base_url() . 'home');

            }
		}

		else {
            $this->session->set_flashdata('error', 'Oops. Something Went Wrong. Please Try Again.');
            header("refresh:1,url=". base_url() ."home");
		}
	}

}
