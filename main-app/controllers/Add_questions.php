<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_questions extends CI_Controller {
	public function __construct()
	{
		$this->details = array();
		parent::__construct();
		$this->load->model('Add_questions_mod');
	}
	public function index()
	{
        $name = $this->session->user_name;
		$avail_quiz = $this->Add_questions_mod->find_all_quiz();

		if(isset($avail_quiz['error'])){
            $this->session->set_flashdata('error', $avail_quiz['error']);
            header("refresh:1,url=". base_url() ."home");
        }

        else {
            $data = array(
                'name' => $name,
                'avail_quizzes' => $avail_quiz,
            );

            $message = $this->session->flashdata('message');

            $error = $this->session->flashdata('error');

            $msg = $this->session->flashdata('msg');

            if(isset($message)){
                $data['message'] = $message;
            }
            if(isset($error)){
                $data['error'] = $error;
            }
            if(isset($msg)){
                $data['msg'] = $msg;
            }


            $this->load->view('static/header_add_edit_question');
            $this->load->view('pages/add_questions', $data);
            $this->load->view('static/footer');
        }
	}

	public function add_new_question()
	{
        $settings =  array(
            'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/",
            'upload_url'      => base_url()."uploads/",
            'allowed_types'   => "gif|jpg|png|jpeg|pdf|doc|xml",
            'overwrite'       => TRUE,
            'encrypt_name'    => TRUE,
            'quality'         => '60%',
//            'wm_text'         => 'Copyright 2018 - QuizSystem',
//            'wm_type' => 'text',
            // 'wm_font_path' => dirname($_SERVER["SCRIPT_FILENAME"])."/assets/fonts/wob.ttf",
//            'wm_font_size' => '52',
//            'wm_font_color' => 'ffffff',
//            'wm_vrt_alignment' => 'bottom',
//            'wm_hor_alignment' => 'center',
//            'wm_padding' => '20',
        );

        $this->load->library('upload', $settings);
        if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
            if($this->upload->do_upload())
            {
                $flag = 1;
                $file_name = $this->upload->data('file_name');

                $config['source_image'] = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/".$file_name;
                $config['wm_text'] = 'Copyright 2018 - QuizSystem';
                $config['wm_type'] = 'text';
                $config['wm_font_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/fonts/wob.ttf";
                $config['wm_font_size'] = '44';
                $config['wm_font_color'] = 'ffffff';
                $config['wm_vrt_alignment'] = 'bottom';
                $config['wm_hor_alignment'] = 'center';
                $config['wm_padding'] = '-20';
                $config['quality'] = '60%';

                $this->load->library('image_lib', $config);

//                echo $file_name;
//                return 0;

                if ( ! $this->image_lib->watermark())
                {
                    // echo 'successful';
                    // echo $file_name;
                    echo $this->image_lib->display_errors();
                    $this->session->set_flashdata('error', $this->image_lib->display_errors());
                    header("refresh:1,url=". base_url() ."Edit_questions");
                    // return 0;
                }

            }
            else
            {
                $flag = 0;
            }
        }
        else{
            $flag = 1;
            $file_name = '';
        }

        if($flag == 1) {

            $this->form_validation->set_rules('quiz_names_avail', 'Available Quizzes', 'required', array('required' => 'You have not provided Quiz Name.'));
            $this->form_validation->set_rules('que', 'Question', 'required', array('required' => 'You have not provided Question Name.'));
            $this->form_validation->set_rules('ans1', 'Answer 1', 'required', array('required' => 'You have to provide atleast one answer in "Answer 1" input.'));
            if ($this->form_validation->run() == TRUE) {
                $data = $this->Add_questions_mod->add_new_questions($file_name);
                if (isset($data['error'])) {
                    $this->session->set_flashdata('error', $data['error']);
                    header("refresh:1,url=" . base_url() . "home");
                }

                if (isset($data['message'])) {
                    $name = $this->session->user_name;
                    $avail_quiz = $this->Add_questions_mod->find_all_quiz();
                    $data2 = array(
                        'name' => $name,
                        'message' => $data['message'],
                        'avail_quizzes' => $avail_quiz,
                        'quiz_name' => $data['quiz_name'],
                    );

                    $this->load->view('static/header_add_edit_question');
                    $this->load->view('pages/add_questions', $data2);
                    $this->load->view('static/footer');
                }
            } else {
                $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again. ');
                header("refresh:1,url=" . base_url() . "home");
            }
        }
        else {
            $this->session->set_flashdata('error', 'File Upload Failed. Please Try Again.');
            header("refresh:1,url=". base_url() ."Edit_questions");
        }

    }

}
