<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_quiz extends CI_Controller {
	public function __construct()
	{
		$this->details = array();
		parent::__construct();
		$this->load->model('Add_quiz_mod');
        $this->load->model('Email_notification_mod');
        $this->load->helper( 'url');
		$this->load->library('session');
	}
	public function index()
	{
	    $message = $this->session->flashdata('message');

        $error = $this->session->flashdata('error');

        $msg = $this->session->flashdata('msg');

        $data = array();

        if(isset($message)){
            $data['message'] = $message;
        }
        if(isset($error)){
            $data['error'] = $error;
        }
        if(isset($msg)){
            $data['msg'] = $msg;
        }

        $this->load->view('static/header');
		$this->load->view('pages/add_quiz',$data);
		$this->load->view('static/footer');
	}

	public function add_new()
	{
		$this->form_validation->set_rules('quiz_name', 'Quiz Name' , 'required' );
		$this->form_validation->set_rules('sub_name', 'Subject Name' , 'required' );
		$this->form_validation->set_rules('unique_id', 'Unique ID' , 'trim|required|alpha_numeric' );
		if($this->form_validation->run() == TRUE)
		{
			$data = $this->Add_quiz_mod->add_new_quiz();
			if(isset($data['error']))
			{
				$data2 = array(
				    'error' => $data['error'],
                );

                $this->load->view('static/header');
                $this->load->view('pages/add_quiz', $data2);
                $this->load->view('static/footer');

            }
            if(isset($data['msg'])){
			    $quiz_name = $data['quiz_name'];
			    $roomname = $data['roomname'];
			    $this->Email_notification_mod->notify_new_quiz($quiz_name, $roomname);

                $this->session->set_flashdata('msg', $data['msg']);
				header("refresh:1,url=". base_url() ."/home");
            }
		}
        else
        {
            $this->session->set_flashdata('error', 'Oops, something went wrong. Please try again. ');
            header("refresh:1,url=". base_url() ."home");
        }
	}

}
