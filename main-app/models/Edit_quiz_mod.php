<?php

class Edit_quiz_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        $this->load->library('session');
        parent::__construct();
    }

    public function find_all_quiz()
    {
        $name = $this->session->user_name;

        $sql1 = "SELECT id FROM admin WHERE user_name= ?";
        $query1 = $this->db->query($sql1, array($name));

        $get_userID = $query1->row();

        if(isset($get_userID)){
            $sql2 = "SELECT quiz_name,roomname FROM quiz WHERE user_id= ? ORDER BY quiz_name ASC";
            $query2 = $this->db->query($sql2, array($get_userID->id));
//            $total_quiz = $query2->affected_rows();
            if (!$query2) {
                $data = array(
                    'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
                );
                return $data;
            }

            else {
                return $query2->result_array();
            }

        }
        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

            return $data;
        }
    }

    public function get_single_quiz_details(){
        $selected_quiz = $this->input->post('quiz_names_avail');
//        echo $selected_quiz;

        $sql1 = "SELECT * FROM quiz WHERE roomname = ?";
        $query1 = $this->db->query($sql1, array($selected_quiz));


        if(isset($query1)){
            return $query1->result_array();
        }
        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong While Fetching The Answers. Please try again',
            );
            return $data;
        }
    }


    public function update_single_quiz()
    {
        $quiz_name = $this->input->post('quiz');
        $subject_name = $this->input->post('sub');
        $roomname = $this->input->post('room');
        $quiz_type = $this->input->post('quiz_type');

        $hrs = $this->input->post('hrs');
        $mins = $this->input->post('mins');
        $secs = $this->input->post('secs');

        if(!$hrs){
            $hrs = 0;
        }
        if(!$mins){
            $mins = 0;
        }
        if(!$secs){
            $secs = 0;
        }


        $total_time = ($hrs*3600) + ($mins*60) + ($secs);


//        echo $roomname;

//        return 0;


        $sql = "UPDATE quiz SET quiz_name = ?, subject_name = ?, time = ?, quiz_type = ? WHERE roomname = ?";
        $query = $this->db->query($sql, array($quiz_name, $subject_name, $total_time ,$quiz_type, $roomname));

        if (!isset($query)) {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        } else {
            $data = array(
                'message' => 'Quiz Updated Successfully.',
            );
            return $data;
        }
    }
}

