<?php

class Add_questions_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
	}

	public function find_all_quiz()
	{
		$name = $this->session->user_name;

		$sql1 = "SELECT id FROM admin WHERE user_name=?";
		$query1 = $this->db->query($sql1, array($name));

		$get_quizID = $query1->row();

		if(isset($get_quizID)){
			$sql2 = "SELECT quiz_name FROM quiz WHERE user_id= ? ORDER BY quiz_name ASC";
			$query2 = $this->db->query($sql2, array($get_quizID->id));
			if (!isset($query2)) {
				$data = array(
					'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
				);
				return $data;
			} else {
				return $query2->result_array();
			}
		}
		else{
		    $data = array(
		        'error' => 'Quiz not found. Please try again.',
            );
		    return $data;
        }
	}


	public function add_new_questions($file_name)
	{
		$quiz_name = $this->input->post('quiz_names_avail');
		$question = $this->input->post('que');
		$ans[1] = $this->input->post('ans1');
		$ans[2] = $this->input->post('ans2');
		$ans[3] = $this->input->post('ans3');
		$ans[4] = $this->input->post('ans4');
		$check[1] = $this->input->post('check1');
		$check[2] = $this->input->post('check2');
		$check[3] = $this->input->post('check3');
		$check[4] = $this->input->post('check4');

        if((int) $this->input->post('partial') == 1)
		    $partial = 1;
        else
            $partial = 0;
		$is_multi = 0;

		if((int) $check[1] == 1 && (int) $check[2] == 1 || (int) $check[1] == 1 && (int) $check[3] == 1 || (int) $check[1] == 1 && (int) $check[4] == 1 || (int) $check[2] == 1 && (int) $check[3] == 1 || (int) $check[2] == 1 && (int) $check[4] == 1 || (int) $check[3] == 1 && (int) $check[4] == 1) {
			$is_multi = 1;
		}

		$sql1 = "SELECT id FROM quiz WHERE quiz_name= ?";
		$query1 = $this->db->query($sql1, array($quiz_name));

		$get_quizID = $query1->row();

		if ($get_quizID) {
			$sql = "INSERT INTO quiz_questions (question, is_multi, quiz_id, partial_marking,img_path) VALUES (?,?,?,?,?)";
			$query = $this->db->query($sql, array($question, $is_multi, $get_quizID->id, $partial,$file_name));

			$sqlUpdateTotal = "UPDATE quiz SET total_questions = total_questions + 1, is_empty = 0 WHERE id = ?";
			$queryUpdateTotal = $this->db->query($sqlUpdateTotal, array($get_quizID->id));

			if (!isset($query) || !isset($queryUpdateTotal)) {
				$data = array(
					'error' => "Something went wrong. Please try again.",
				);
				return $data;
			}

			else {

				$sql3 = "SELECT id FROM quiz_questions WHERE question= ?";
				$query3 = $this->db->query($sql3, array($question));

				$get_questionID = $query3->row();

                if (!$get_questionID) {
                    $data = array(
                        'error' => "Something went wrong. Please try again.",
                    );
                    return $data;
                }

                else {
                    for ($i = 1; $i < 5; $i++) {
                        if (isset($ans[$i])) {
                            if (!isset($check[$i])) {
                                $check[$i] = 0;
                            }
                            $sql2 = "INSERT INTO quiz_answers (answer, ques_id, is_true, quiz_id) VALUES (?,?,?,?)";
                            $query2 = $this->db->query($sql2, array($ans[$i], $get_questionID->id, $check[$i], $get_quizID->id));
                        }
                    }

                    if (isset($query2)) {
                        $data = array(
                            'quiz_name' => $quiz_name,
                            'message' => 'Question Added Successfully',
                        );
                        return $data;
                    }

                    else {
                        $data = array(
                            'error' => 'Something went wrong',
                        );
                        return $data;
                    }
                }
			}
		}
        else{
            $data = array(
                'error' => 'Quiz not found. Please try again.',
            );
            return $data;
        }
    }
}

