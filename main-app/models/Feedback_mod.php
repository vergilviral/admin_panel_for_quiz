<?php

class Feedback_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        $this->load->library('session');
        parent::__construct();
    }

    public function submit_feedback()
    {
        $name = $this->session->user_name;
        $email = $this->session->email;
        $feedback = $this->input->post('feedback');
        $agree = $this->input->post('agreement');

        if((int)$agree == 1){
            $agree = 'Yes';
        }
        else{
            $agree = 'No';
        }


        $sql1 = "INSERT INTO feedbacks (username, email, feedback, contact_back, source) VALUES (?,?,?,?,'Admin Panel')";
        $query1 = $this->db->query($sql1, array($name, $email, $feedback, $agree));

        if(isset($query1)){
            $data = array(
                'name' => $name,
                'email' => $email,
                'feedback' => $feedback,
                'agree' => $agree,
                'message' => 'Feedback successfully submitted. Thank you.',
            );

            return $data;
        }

        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        }
    }
}

