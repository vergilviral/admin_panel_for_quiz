<?php

class Edit_questions_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		$this->load->library('session');
		parent::__construct();
	}

	public function find_all_quiz()
	{
		$name = $this->session->user_name;

		$sql1 = "SELECT id FROM admin WHERE user_name= ?";
		$query1 = $this->db->query($sql1, array($name));

		$get_userID = $query1->row();

		if(isset($get_userID)){
			$sql2 = "SELECT quiz_name FROM quiz WHERE user_id= ? ORDER BY quiz_name ASC";
			$query2 = $this->db->query($sql2, array($get_userID->id));
//            $total_quiz = $query2->affected_rows();
			if (!$query2) {
				$data = array(
					'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
				);
				return $data;
			}

			else {
				return $query2->result_array();
			}

		}
		else {
		    $data = array(
		        'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

		    return $data;
        }
	}

	public function list_questions(){

		if($this->input->post('quiz_names_avail')) {
			$quiz_name = $this->input->post('quiz_names_avail');
            $sess_data = array(
                'quiz_name' => $quiz_name,
            );
            $this->session->set_userdata($sess_data);
        }

		else {
			$quiz_name = $this->session->userdata('quiz_id');
		}

		$user_id = $this->session->user_id;

		$sql1 = "SELECT id FROM quiz WHERE quiz_name= ? AND user_id = ?";
		$query1 = $this->db->query($sql1, array($quiz_name, $user_id));

		$get_quizID = $query1->row();

		if(isset($get_quizID)){

			$sess_data = array(
				'quiz_id' => $get_quizID->id,
			);
			$this->session->set_userdata($sess_data);


			$sql2 = "SELECT question,id FROM quiz_questions WHERE quiz_id= ?";
			$query2 = $this->db->query($sql2, array($get_quizID->id));

			if(isset($query2)){
				return $query2->result_array();
			}

			else {
			    $data = array(
			        'error' => 'No Questions Found In This Quiz. Please Add The Question First.'
                );

			    return $data;
            }
		}

		else {
		    $data = array(
		        'error' => 'Quiz Not Found. Please Try Again.',
            );

		    return $data;
        }
	}

	public function get_single_question_details(){
		$selected_question = $this->input->post('selected_question');
        $quiz_id = $this->session->quiz_id;

        // echo $selected_question;
        // echo $quiz_id;

//        echo $quiz_name;

		$sql1 = "SELECT id, partial_marking FROM quiz_questions WHERE question = ? AND quiz_id= ?";
		$query1 = $this->db->query($sql1, array($selected_question, $quiz_id));

		$get_questionID = $query1->row();

// 		echo $get_questionID->id;

		if(isset($get_questionID->id)){
			$sess_data = array(
				'q_id' => $get_questionID->id,
			);
			$this->session->set_userdata($sess_data);

            $sql3 = "SELECT img_path FROM quiz_questions WHERE id= ?";
            $query3 = $this->db->query($sql3, array($get_questionID->id));

            $get_img_path = $query3->row();

            if($get_img_path) {

                $img_path = $get_img_path->img_path;

                $this->session->set_flashdata('img_path', $img_path);


                $sql2 = "SELECT answer,is_true FROM quiz_answers WHERE ques_id= ?";
                $query2 = $this->db->query($sql2, array($get_questionID->id));

                if (isset($query2)) {
                    $data = array(
                        'answers' => $query2->result_array(),
                        'partial_marking' => $get_questionID->partial_marking
                    );
                    return $data;
                }

                else {
                    $data = array(
                        'error' => 'Oops. Something Went Wrong While Fetching The Answers. Please try again',
                    );
                    return $data;

                }
            }
            else {
                $data = array(
                    'error' => 'Oops. Something went wrong. Please try again.',
                );
                return $data;
            }

		}
		else {
		    $data = array(
		        'error' => 'Oops. Something went wrong. Please try again.',
            );

		    return $data;
        }
	}


	public function update_single_question($file_name)
	{
		$question_id = $this->session->userdata('q_id');
		$question = $this->input->post('que');
		$ans[1] = $this->input->post('ans1');
		$ans[2] = $this->input->post('ans2');
		$ans[3] = $this->input->post('ans3');
		$ans[4] = $this->input->post('ans4');
		$check[1] = $this->input->post('check1');
		$check[2] = $this->input->post('check2');
		$check[3] = $this->input->post('check3');
		$check[4] = $this->input->post('check4');
		$is_multi = 0;

        if((int) $this->input->post('partial') == 1)
            $partial = 1;
        else
            $partial = 0;

        if ((int)$check[1] == 1 && (int)$check[2] == 1 || (int)$check[1] == 1 && (int)$check[3] == 1 || (int)$check[1] == 1 && (int)$check[4] == 1 || (int)$check[2] == 1 && (int)$check[3] == 1 || (int)$check[2] == 1 && (int)$check[4] == 1 || (int)$check[3] == 1 && (int)$check[4] == 1) {
			$is_multi = 1;
		}

		if (isset($question_id)) {

			$sql = "UPDATE quiz_questions SET question = ?, is_multi = ?, img_path = ?, partial_marking = ? WHERE id = ?";
			$query = $this->db->query($sql, array($question, $is_multi, $file_name, $partial, $question_id));


			if (!isset($query)) {
                $data = array(
                    'error' => 'Oops. Something Went Wrong. Please Try Again.',
                );

                return $data;
			}

			else {
				$sql3 = "SELECT id FROM quiz_questions WHERE question= ?";
				$query3 = $this->db->query($sql3, array($question));

				$get_questionID = $query3->row();

				if($get_questionID) {
                    for ($i = 1; $i < 5; $i++) {
                        if (isset($ans[$i])) {
                            if (!isset($check[$i])) {
                                $check[$i] = 0;
                            }

                            $sql4 = "SELECT ans_id FROM quiz_answers WHERE ques_id= ?";
                            $query4 = $this->db->query($sql4, array($get_questionID->id));

                            $current_answers_array = $query4->result_array();

                            $current_loop_answer = $current_answers_array[$i - 1];

                            $sql2 = "UPDATE quiz_answers SET answer = ?, is_true = ? WHERE ans_id = ?";
                            $query2 = $this->db->query($sql2, array($ans[$i], $check[$i], $current_loop_answer['ans_id']));
                        }
                    }

                    if (isset($query2)) {
                        $data = array(
                            'message' => 'Question Updated Successfully.',
                        );
                        return $data;
                    }

                    else {
                        $data = array(
                            'error' => 'Oops. Something went wrong. Please Try Again.',
                        );
                        return $data;
                    }
                }
                else {
                    $data = array(
                        'error' => 'Oops. Something Went Wrong. Please Try Again.',
                    );

                    return $data;
                }
			}
		}
		else {
		    $data = array(
		        'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

		    return $data;
        }
	}
}

