<?php

class Edit_profile_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
	}

	public function get_single_user_data()
	{

		$name = $this->session->user_name;

		$sql1 = "SELECT * FROM admin WHERE user_name= ?";
		$query1 = $this->db->query($sql1, array($name));

		$get_userID = $query1->row();

		$profile_pic = $get_userID->profile_pic_path;

        $this->session->set_userdata('profile_pic', $profile_pic);

        if ($get_userID) {
			return $query1->result_array();
		}

		else {
            $data = array(
                'error' => 'Your session has been expired. Please log in again '
            );

            return $data;
        }
	}

	public function update_profile($file_name)
	{
		$name = $this->session->user_name;
		$pass = '';
		$flag = 0;
		$flag_file = 0;

		if($file_name == ''){
//            $file_name = 'default_avatar.png';
//            echo $file_name;
//            echo $x;
            $flag_file = 0;
        }

        if($file_name != ''){
            $flag_file = 1;
        }


		if($this->input->post('email')){
			$email = $this->input->post('email');
        }

		if($this->input->post('first_name')){
			$first_name = $this->input->post('first_name');
		}

		if($this->input->post('last_name')){
			$last_name = $this->input->post('last_name');
		}

		if($this->input->post('pass')){
			$pass = $this->input->post('pass');
            $password = password_hash($pass, PASSWORD_DEFAULT);

        }

		if(isset($email)) {
            $sqlemail = "SELECT user_email FROM admin WHERE user_email= ? AND user_name != ?";
            $queryemail = $this->db->query($sqlemail, array($email, $name));

            $check_email = $queryemail->row();

            if($check_email){
                $data = array(
                    'error' => 'Email already exists. Please register with different email.',
                );
                return $data;
            }
        }

        if(isset($email)) {
            $sqlemail = "SELECT user_email FROM admin WHERE user_name = ?";
            $queryemail = $this->db->query($sqlemail, array($name));

            $check_email = $queryemail->row();

            if($check_email->user_email == $email){
                $flag = 1;
            }
            else {
                $flag = 0;
            }
        }

        if($flag_file == 1) {
            $sql = "UPDATE admin SET user_email = ?, first_name = ?, last_name = ?, profile_pic_path = ? WHERE user_name = ?";
            $query = $this->db->query($sql, array($email, $first_name, $last_name, $file_name, $name));
        }
        else if ($flag_file == 0){
            $sql = "UPDATE admin SET user_email = ?, first_name = ?, last_name = ?  WHERE user_name = ?";
            $query = $this->db->query($sql, array($email, $first_name, $last_name, $name));
        }

		if ($query) {

			if(!empty($pass)){
				$sql2 = "UPDATE admin SET user_pass = ? WHERE user_name = ?";
				$query2 = $this->db->query($sql2, array($password, $name));

				if($query2){
					$data = array(
						'message' => 'User Profile Successfully Updated',
                        'pass_changed' => '1',
					);
					if($flag == 0) {
					    $data['email_changed'] = 1;
                    }
                    return $data;
                }
            }
			else{
                $data = array(
                    'message' => 'User Profile Successfully Updated',
                );
                if($flag == 0) {
                    $data['email_changed'] = 1;
                }
                return $data;

            }
            $data = array(
                'message' => 'User Profile Successfully Updated',
            );
            return $data;
        }

        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );
            return $data;
        }
    }

}

