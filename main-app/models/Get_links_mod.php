<?php

class Get_links_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		$this->load->library('session');
		parent::__construct();
	}

	public function find_all_quiz()
	{
		$name = $this->session->user_name;

		$sql1 = "SELECT id FROM admin WHERE user_name= ?";
		$query1 = $this->db->query($sql1, array($name));

		$get_quizID = $query1->row();

		if(isset($get_quizID)){
			$sql2 = "SELECT title,keyword FROM yourls_url WHERE user_id= ? ORDER BY title ASC";
			$query2 = $this->db->query($sql2, array($get_quizID->id));
			if (!isset($query2)) {
				$data = array(
					'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
				);
				return $data;
			}

			else {
				return $query2->result_array();
			}

		}

		else {
		    $data = array(
		        'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

		    return $data;
        }
	}
}

