<?php

class Home_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
	}

	public function get_quiz_info()
	{
		$name = $this->session->user_name;

		$sql1 = "SELECT id FROM admin WHERE user_name= ?";
		$query1 = $this->db->query($sql1, array($name));

		$get_userID = $query1->row();

		if ($get_userID) {
			$sql = "SELECT quiz_name,total_students_joined,total_rights,total_questions FROM quiz WHERE user_id= ?";
			$query = $this->db->query($sql, array($get_userID->id));

			if (!$query) {
				$data = array(
					'error' => 'Something Went wrong. Please try again.',
				);
				return $data;
			}

			else {
				return $query->result_array();
			}
		}

		else {
		    $data = array(
		        'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

		    return $data;
        }
	}
}

