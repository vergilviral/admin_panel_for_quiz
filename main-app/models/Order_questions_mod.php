<?php

class Order_questions_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        $this->load->library('session');
        parent::__construct();
    }

    public function find_all_quiz()
    {
        $name = $this->session->user_name;

        $sql1 = "SELECT id FROM admin WHERE user_name= ?";
        $query1 = $this->db->query($sql1, array($name));

        $get_userID = $query1->row();

        if(isset($get_userID)){
            $sql2 = "SELECT quiz_name FROM quiz WHERE user_id= ? ORDER BY quiz_name ASC";
            $query2 = $this->db->query($sql2, array($get_userID->id));
//            $total_quiz = $query2->affected_rows();
            if (!$query2) {
                $data = array(
                    'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
                );
                return $data;
            }

            else {
                return $query2->result_array();
            }

        }
        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

            return $data;
        }
    }

    public function list_questions(){

        if($this->input->post('quiz_names_avail')) {
            $quiz_name = $this->input->post('quiz_names_avail');
            $sess_data = array(
                'quiz_name' => $quiz_name,
            );
            $this->session->set_userdata($sess_data);
        }

        else {
            $quiz_name = $this->session->userdata('quiz_id');
        }

        $user_id = $this->session->user_id;

        $sql1 = "SELECT id FROM quiz WHERE quiz_name= ? AND user_id = ?";
        $query1 = $this->db->query($sql1, array($quiz_name, $user_id));

        $get_quizID = $query1->row();

        if(isset($get_quizID)){

            $sess_data = array(
                'quiz_id' => $get_quizID->id,
            );
            $this->session->set_userdata($sess_data);


            $sql2 = "SELECT question,id FROM quiz_questions WHERE quiz_id= ?";
            $query2 = $this->db->query($sql2, array($get_quizID->id));

            if(isset($query2)){
                return $query2->result_array();
            }

            else {
                $data = array(
                    'error' => 'No Questions Found In This Quiz. Please Add The Question First.'
                );

                return $data;
            }
        }

        else {
            $data = array(
                'error' => 'Quiz Not Found. Please Try Again.',
            );

            return $data;
        }
    }

    function change_values_of_order($old_order,$new_order,$changed_order,$back, $old_order_back){
        if($old_order_back > 0){
            $key = array_search($back, $new_order);
            if($old_order_back == 1){
                $sql = "UPDATE quiz_questions SET id = ? WHERE id = 1562";
                $query = $this->db->query($sql, array($back));
            }
            else{
                $sql = "UPDATE quiz_questions SET id = ? WHERE id = ?";
                $query = $this->db->query($sql, array($back, $old_order[$key]));
            }

            if (!isset($query)) {
                $data = array(
                    'error' => 'Oops. Something Went Wrong. Please Try Again.',
                );

                return $data;
            }

            else {
                $back = $old_order[$key];
                $old_order_back--;
                $this->change_values_of_order($old_order,$new_order,$changed_order,$back, $old_order_back);
            }
        }
        else{
            $data = array(
                'message' => 'Order of questions updated successfully.',
            );
            return $data;
        }
    }

    public function update_order()
    {
        $old_order = explode(',', $this->input->post('mainorder'));
        $new_order = $old_order;
        sort($new_order);

        for ($i = 0; $i < count($old_order); $i++) {
            if ($old_order[$i] == $new_order[$i]) {
                unset($old_order[$i]);
                unset($new_order[$i]);
            }
        }

        $old_order_back = count($old_order);
        $changed_order = [];

        // step 1

        $key = array_search(max($old_order), $old_order);

        // step 2
        // $changed_order[$key] = 999;

        $sql = "UPDATE quiz_questions SET id = ? WHERE id = ?";
        $query = $this->db->query($sql, array(1562, max($old_order)));


        if (!isset($query)) {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        } else {
            // step 3
//          $key = array_search(end($old_order), $old_order);


            //step 4
//          $changed_order[$key] = max($old_order);
            $sql = "UPDATE quiz_questions SET id = ? WHERE id = ?";
            $query = $this->db->query($sql, array(max($old_order), end($old_order)));
            $back = end($old_order);

            if (!isset($query)) {
                $data = array(
                    'error' => 'Oops. Something Went Wrong. Please Try Again.',
                );

                return $data;
            } else {
                // step 5
//                $key = array_search($back, $new_order);
//                $changed_order[$key] = $back;
//                $back = $old_order[$key];
//
                $old_order_back--;
                $this->change_values_of_order($old_order, $new_order, $changed_order, $back, $old_order_back);
            }
        }
    }
}

