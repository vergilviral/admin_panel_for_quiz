<?php

class Delete_question_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        parent::__construct();
    }

    public function delete()
    {
        if ($this->input->post('selected_question')) {
            $question_id = $this->input->post('selected_question');
        }
        else {
            $question_id = $this->session->userdata('selected_question');
        }

        $sql1 = "SELECT question FROM quiz_questions WHERE id= ?";
        $query1 = $this->db->query($sql1, array($question_id));

        $get_question_name = $query1->row();

        if(isset($get_question_name)) {

            $sql2 = "DELETE FROM quiz_answers WHERE ques_id= ?";
            $query2 = $this->db->query($sql2, array($question_id));

            if ($query2) {
                $sql3 = "DELETE FROM quiz_questions WHERE id= ?";
                $query3 = $this->db->query($sql3, array($question_id));

                if ($query3) {
                    $data = array(
                        'msg' => $get_question_name->question . ' Successfully Deleted.',
                    );
                    return $data;
                }
                else {
                    $data = array(
                        'error' => 'Oops Something Went Wrong. Please Try Again.',
                    );

                    return $data;
                }
            }
            else {
                $data = array(
                    'error' => 'Oops Something Went Wrong. Please Try Again.',
                );

                return $data;
            }
        }

        else {
            $data = array(
                'error' => 'Oops Something Went Wrong. Please Try Again.',
            );

            return $data;
        }
    }
}

