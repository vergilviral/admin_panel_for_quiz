<?php

class Add_quiz_mod extends CI_Model{

	function __construct() {
		$this->details = array();
		parent::__construct();
//		$this->load->library('encryption');
	}

	public function add_new_quiz()
	{
		$quiz_name = $this->input->post('quiz_name');
		$sub_name = $this->input->post('sub_name');
		$unique_id = $this->input->post('unique_id');
		$quiz_type = $this->input->post('quiz_type');
		$name = $this->session->user_name;
		$hrs = $this->input->post('hrs');
		$mins = $this->input->post('mins');
		$secs = $this->input->post('secs');

		if(!$hrs){
		    $hrs = 0;
        }
        if(!$mins){
            $mins = 0;
        }
        if(!$secs){
            $secs = 0;
        }


		$total_time = ($hrs*3600) + ($mins*60) + ($secs);

		$sql1 = "SELECT id FROM admin WHERE user_name= ?";
		$query1 = $this->db->query($sql1, array($name));

		$get_userID = $query1->row();

		if ($get_userID) {
			$sql = "SELECT quiz_name FROM quiz WHERE roomname= ?";
			$query = $this->db->query($sql, array($unique_id));

			$check_roomname = $query->row();

			if ($check_roomname) {
				$data = array(
					'error' => 'This unique ID is already assigned to other quiz. Please enter different ID and try again.',
				);
				return $data;
			}

			else {
				$sql2 = "INSERT INTO quiz (quiz_name, subject_name, roomname, user_id, quiz_type,is_empty,time) VALUES (?,UPPER(?),?,?,?,?,?)";
				$query2 = $this->db->query($sql2, array($quiz_name, $sub_name, $unique_id, $get_userID->id, $quiz_type,1,$total_time));

				$add_new_quiz = $query2;

				if ($add_new_quiz) {

//				    echo ('http://vedaquiz.com/?ns=' . $unique_id);
                    $sql3 = "INSERT INTO yourls_url (url, title, user_id) VALUES (?,?,?)";
                    $query3 = $this->db->query($sql3, array('http://vedaquiz.com/?ns=' . $unique_id, $quiz_name, $get_userID->id));

                    if($query3){
                        $data = array(
                            'msg' => 'Quiz Successfully Added With Name: ' . $quiz_name,
                            'quiz_name' => $quiz_name,
                            'roomname' => $unique_id,
                        );

                        return $data;
                    }
                    else {
                        $data = array(
                            'error' => 'Whoops. Something went wrong. Please try again.',
                        );

                        return $data;
                    }
				}

				else {
                    $data = array(
                        'error' => 'Whoops. Something went wrong. Please try again.',
                    );

                    return $data;
				}
			}
		}
		else {
		    $data = array(
		        'error' => 'Whoops. Something went wrong. Please try again.',
            );

		    return $data;
        }
	}

}

