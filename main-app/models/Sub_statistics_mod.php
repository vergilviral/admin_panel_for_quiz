<?php

class Sub_statistics_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        parent::__construct();
    }

    public function find_all_subjects()
    {
        $name = $this->session->user_name;

        $sql1 = "SELECT id FROM admin WHERE user_name=?";
        $query1 = $this->db->query($sql1, array($name));

        $get_quizID = $query1->row();

        if(isset($get_quizID)){
            $sql2 = "SELECT subject_name FROM quiz WHERE user_id= ? ORDER BY subject_name ASC";
            $query2 = $this->db->query($sql2, array($get_quizID->id));
            if (!isset($query2)) {
                $data = array(
                    'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
                );
                return $data;
            } else {
                return $query2->result_array();
            }
        }
        else{
            $data = array(
                'error' => 'Quiz not found. Please try again.',
            );
            return $data;
        }
    }


    public function get_stats()
    {

        $sub_name = $this->input->post('sub_names_avail');

        $name = $this->session->user_name;

        $sql1 = "SELECT id FROM admin WHERE user_name= ?";
        $query1 = $this->db->query($sql1, array($name));

        $get_userID = $query1->row();

        if ($get_userID) {
            $sql = "SELECT quiz_name,total_students_joined,total_rights,total_questions FROM quiz WHERE user_id= ? AND subject_name= ?";
            $query = $this->db->query($sql, array($get_userID->id, $sub_name));

            if (!$query) {
                $data = array(
                    'error' => 'Something Went wrong. Please try again.',
                );
                return $data;
            } else {
                return $query->result_array();
            }
        } else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.'
            );

            return $data;
        }
    }
}

