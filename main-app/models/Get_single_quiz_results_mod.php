<?php

class Get_single_quiz_results_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        $this->load->library('session');
        parent::__construct();
    }

    public function find_all_quiz()
    {
        $name = $this->session->user_name;

        $sql1 = "SELECT id FROM admin WHERE user_name= ?";
        $query1 = $this->db->query($sql1, array($name));

        $get_quizID = $query1->row();

        if(isset($get_quizID)){
            $sql2 = "SELECT quiz_name,roomname FROM quiz WHERE user_id= ? ORDER BY quiz_name ASC";
            $query2 = $this->db->query($sql2, array($get_quizID->id));
            if (!isset($query2)) {
                $data = array(
                    'error' => "You don't have any quiz. Please add quiz first and then come back to add questions.",
                );
                return $data;
            }

            else {
                return $query2->result_array();
            }

        }
        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        }
    }

    public function get_single_result()
    {

        $roomname = $this->input->post('roomname');

        $sql1 = "SELECT id FROM quiz WHERE roomname = ?";
        $query1 = $this->db->query($sql1, $roomname);
        $get_quizID = $query1->row();

        if(isset($get_quizID)){
            $sql2 = "SELECT username,total_score FROM final_results WHERE quiz_id= ?";
            $query2 = $this->db->query($sql2, array($get_quizID->id));
            if (!isset($query2)) {
                $data = array(
                    'error' => "No Results. No one has given this quiz yet.",
                );
                return $data;
            }

            else {
                return $query2->result_array();
            }

        }

        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        }
    }

    public function get_quiz_name()
    {

        $roomname = $this->input->post('roomname');

        $sql1 = "SELECT quiz_name FROM quiz WHERE roomname = ?";
        $query1 = $this->db->query($sql1, $roomname);
        $get_quizName = $query1->row();

        if($get_quizName) {
            return $get_quizName->quiz_name;
        }
        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong. Please Try Again.',
            );

            return $data;
        }

    }

}

