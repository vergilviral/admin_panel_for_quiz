<?php

class Forgot_password_mod extends CI_Model
{

    function __construct()
    {
        $this->details = array();
        parent::__construct();
//		$this->load->library('encryption');
        $this->load->helper('string');
    }

    public function send_details()
    {
        $username = $this->input->post('username');
        $random_pass = random_string('alnum', 8);

        $password = password_hash($random_pass, PASSWORD_DEFAULT);

        $sql1 = "SELECT id,user_email FROM admin WHERE user_name= ?";
        $query1 = $this->db->query($sql1, array($username));
        
        $q1 = $query1->row();

        if (isset($q1->id)) {

            $sql2 = "UPDATE admin SET user_pass = ? WHERE user_name= ?";
            $query2 = $this->db->query($sql2, array($password, $username));

            if ($query2) {
                $data = array(
                    'msg' => 'Please check your email for temporary password and further instructions.',
                    'random_pass' => $random_pass,
                    'email' => $q1->user_email,
                );

                return $data;
            }

            else {
                $data = array(
                    'error' => 'Oops. Something went wrong. Please try again.',
                );
                return $data;
            }
        }

        else {

            $sql3 = "SELECT id,user_email FROM admin WHERE user_email= ?";
            $query3 = $this->db->query($sql3, array($username));

            $q3 = $query3->row();

            if ($q3) {

                $sql4 = "UPDATE admin SET user_pass = ? WHERE user_email= ?";
                $query4 = $this->db->query($sql4, array($password, $username));
                
                $q4 = $query3->row();
                
                if ($q4) {
                    $data = array(
                        'msg' => 'Please check your email for temporary password and further instructions.',
                        'random_pass' => $random_pass,
                        'email' => $q3->user_email,
                    );
                    return $data;
                }

                else {
                    $data = array(
                        'error' => 'Oops. Something went wrong. Please try again.',
                    );
                    return $data;
                }
            }

            else {
                $data = array(
                    'error' => 'Username or email ID is not registered.',
                );
                return $data;
            }
        }
    }
}

