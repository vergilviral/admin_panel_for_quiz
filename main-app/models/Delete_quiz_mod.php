<?php

class Delete_quiz_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        parent::__construct();
    }

    public function delete()
    {
        $name = $this->session->user_name;

        if ($this->input->post('quiz_names_avail')) {
            $quiz_name = $this->input->post('quiz_names_avail');
        } else {
            $quiz_name = $this->session->userdata('quiz_name');
        }

        $sql = "SELECT id FROM admin WHERE user_name= ?";
        $query = $this->db->query($sql, array($name));

        $get_userID = $query->row();

        if(isset($query)) {

            $sql1 = "SELECT id FROM quiz WHERE quiz_name= ? AND user_id = ?";
            $query1 = $this->db->query($sql1, array($quiz_name, $get_userID->id));

            $get_quizID = $query1->row();

            if($get_quizID){
                $sql2 = "DELETE FROM final_results WHERE quiz_id= ?";
                $query2 = $this->db->query($sql2, array($get_quizID->id));

                $sql3 = "DELETE FROM quiz_answers WHERE quiz_id= ?";
                $query3 = $this->db->query($sql3, array($get_quizID->id));

                $sql4 = "DELETE FROM quiz_questions WHERE quiz_id= ?";
                $query4 = $this->db->query($sql4, array($get_quizID->id));

                $sql5 = "DELETE FROM quiz WHERE quiz_name= ? AND user_id = ?";
                $query5 = $this->db->query($sql5, array($quiz_name, $get_userID->id));

                if($query2 && $query3 && $query4 && $query5){
                    $data = array(
                        'msg' => $quiz_name . ' Successfully Deleted.',
                        'quiz_name' => $quiz_name,
                    );
                    return $data;
                }
                else {
                    $data = array(
                        'error' => 'Oops. Something Went Wrong. Please Try Again.'
                    );

                    return $data;
                }
            }
            else {
                $data = array(
                    'error' => 'Oops. Something Went Wrong While Getting Quiz Data. Please Try Again.'
                );

                return $data;
            }
        }

        else {
            $data = array(
                'error' => 'Oops. Something Went Wrong While Checking Your Session. Please Log Out and Login Again.'
            );

            return $data;
        }
    }
}