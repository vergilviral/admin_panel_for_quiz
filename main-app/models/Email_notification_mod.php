<?php

class Email_notification_mod extends CI_Model{

    function __construct() {
        $this->details = array();
        parent::__construct();
        $this->load->helper('security');

    }

    public function notify_new_quiz($quiz_name, $roomname)
    {
        $email = $this->session->email;


        $this->email->from('support@vedaquiz.com', 'VedaQuiz');
        $this->email->to($email);

        $this->email->subject('New quiz has been created with name: ' . $quiz_name);

        $this->email->message("Dear User,<br/>You have created new quiz with the name " . $quiz_name .".<br/><br/> You can use the following link to join / start the quiz. <br/><br/> Share this link to respective participants so that they can join your quiz.<br/><br/>http://vedaquiz.it/?ns=".$roomname."<br/>"."<br/><br/>Thanks<br/>Admin Team ");

        $this->email->send();

    }

    public function notify_delete_quiz($quiz_name)
    {
        $email = $this->session->email;


        $this->email->from('support@vedaquiz.com', 'VedaQuiz');
        $this->email->to($email);

        $this->email->subject('Quiz deleted with name: ' . $quiz_name);

        $this->email->message("Dear User,<br/>You deleted the quiz with the name " . $quiz_name ."<br/><br/>You can always create new quiz if you like by going to Admin Panel -> Manage Quiz -> Add Quiz <br/><br/>Thanks<br/>Admin Team ");

        $this->email->send();

    }

    public function password_changed()
    {
        $email = $this->session->email;


        $this->email->from('support@vedaquiz.com', 'VedaQuiz');
        $this->email->to($email);

        $this->email->subject('Your password has been changed');

        $this->email->message("Dear User,<br/>Your password has been changed as per your request.<br/><br/>If you did not change your password, please immediately contact us or recover password through forgot password here: ". base_url() ."Forgot_password <br/><br/>Thanks<br/>Admin Team ");

        $this->email->send();

    }

    public function email_changed()
    {
        $email = $this->session->email;


        $this->email->from('support@vedaquiz.com', 'VedaQuiz');
        $this->email->to($email);

        $this->email->subject('Your email address has been changed');

        $this->email->message("Dear User,<br/>Your email address has been changed has been changed as per your request.<br/><br/>If you did not change your email address, please immediately contact us.<br/><br/> Changes will take effect from your next login.<br/><br/>Thanks<br/>Admin Team ");

        $this->email->send();

    }

    public function forgot_password($tempPass,$email)
    {
        // $email = $this->session->email;


        $this->email->from('support@vedaquiz.com', 'VedaQuiz');
        $this->email->to($email);

        $this->email->subject('Request for password recovery');

        $this->email->message("Dear User,<br/>Your temporary password is: ". $tempPass ."<br/><br/>Please login with this password and then change it from Edit Profile menu.<br/><br/>Thanks<br/>Admin Team ");

        $this->email->send();

    }

    public function new_feedback($feedback)
    {
        // $email = $this->session->email;


        $this->email->from($feedback['email'], $feedback['name']);

        $this->email->to('vedaquiz@gmail.com');

        $this->email->subject('New Feedback Submitted From Admin Panel');

        $this->email->message("Name: ". $feedback['name'] ."<br/><br/>Email ID: ". $feedback['email'] ."<br/><br/>Message: ". $feedback['feedback'] ."<br/><br/>Thanks\nAdmin Team ");

        $this->email->send();

    }



}

