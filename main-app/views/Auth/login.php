<?php
if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg . "', 'success');};</script>";
}
if(isset($error)){
//    echo $error;
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error . "', 'danger');};</script>";
}
if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message . "', 'success');};</script>";
}


?>
<div class="branding">
    <img class="img-fluid computer brand" src="./assets/img/vqlogodark.png" />
    <img class="img-fluid mobile brand" src="./assets/img/vqlogomobdark.png" />
</div>
    <div class="materialContainer">


    <div class="box">

        <div class="title">Login</div>
        <form action="<?php echo base_url(); ?>AuthCtrl/login_verify" method="post">
            <div class="input">
                <label for="username">Username / E-mail</label>
                <input type="text" name="username" id="username" required oninvalid="setCustomValidity('Please enter username')" onchange="valuesChanged();" onkeyup="valuesChanged();">
                <span class="spin"></span>
            </div>

            <div class="input">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" required oninvalid="setCustomValidity('Please enter password.')" onchange="valuesChanged();" onkeyup="valuesChanged();">
                <span class="spin"></span>
            </div>

            <div class="button login">
                <button type="submit" id="subbut"><span>Submit</span> <i class="fa fa-check"></i></button>
            </div>
        </form>

<!--        <a href="--><?php //echo base_url(); ?><!--AuthCtrl/register" class="pass-forgot">Register</a>-->
<!--        <a href="" class="pass-forgot">&nbsp;</a>-->
        <a href="<?php echo base_url(); ?>Forgot_password" class="pass-forgot">Forgot your password?</a>

    </div>

</div>

<footer class="footer" style="position: absolute; bottom: 0px; right: 20px;">
    <div class="container-fluid">
        <p class="copyright pull-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="http://vedaquiz.com/home" target="_blank">VedaQuiz</a>, made with love for better classrooms
        </p>
    </div>
</footer>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript">
    function valuesChanged() {
        try{setCustomValidity('')}catch(e){};
        if($("#username").val() != '' && $("#password").val() != ''){
            $("#subbut").css('color', '#ED2553');
            $("#subbut").css('border-color', '#ED2553');
        }
        else {
            $("#subbut").css('color', 'rgba(0, 0, 0, 0.2)');
            $("#subbut").css('border-color', 'rgba(0, 0, 0, 0.1)');
        }
    }

    $(function() {

        $(".input input").focus(function() {

            $(this).parent(".input").each(function() {
                $("label", this).css({
                    "line-height": "18px",
                    "font-size": "18px",
                    "font-weight": "100",
                    "top": "0px"
                })
                $(".spin", this).css({
                    "width": "100%"
                })
            });
        }).blur(function() {
            $(".spin").css({
                "width": "0px"
            })
            if ($(this).val() == "") {
                $(this).parent(".input").each(function() {
                    $("label", this).css({
                        "line-height": "60px",
                        "font-size": "24px",
                        "font-weight": "300",
                        "top": "10px"
                    })
                });

            }
        });

        $(".button").click(function(e) {
            var pX = e.pageX,
                pY = e.pageY,
                oX = parseInt($(this).offset().left),
                oY = parseInt($(this).offset().top);

            $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
            $('.x-' + oX + '.y-' + oY + '').animate({
                "width": "500px",
                "height": "500px",
                "top": "-250px",
                "left": "-250px",

            }, 600);
            $("button", this).addClass('active');
        })

        $(".alt-2").click(function() {
            if (!$(this).hasClass('material-button')) {
                $(".shape").css({
                    "width": "100%",
                    "height": "100%",
                    "transform": "rotate(0deg)"
                })

                setTimeout(function() {
                    $(".overbox").css({
                        "overflow": "initial"
                    })
                }, 600)

                $(this).animate({
                    "width": "140px",
                    "height": "140px"
                }, 500, function() {
                    $(".box").removeClass("back");

                    $(this).removeClass('active')
                });

                $(".overbox .title").fadeOut(300);
                $(".overbox .input").fadeOut(300);
                $(".overbox .button").fadeOut(300);

                $(".alt-2").addClass('material-buton');
            }

        })

        $(".material-button").click(function() {

            if ($(this).hasClass('material-button')) {
                setTimeout(function() {
                    $(".overbox").css({
                        "overflow": "hidden"
                    })
                    $(".box").addClass("back");
                }, 200)
                $(this).addClass('active').animate({
                    "width": "700px",
                    "height": "700px"
                });

                setTimeout(function() {
                    $(".shape").css({
                        "width": "50%",
                        "height": "50%",
                        "transform": "rotate(45deg)"
                    })

                    $(".overbox .title").fadeIn(300);
                    $(".overbox .input").fadeIn(300);
                    $(".overbox .button").fadeIn(300);
                }, 700)

                $(this).removeClass('material-button');

            }

            if ($(".alt-2").hasClass('material-buton')) {
                $(".alt-2").removeClass('material-buton');
                $(".alt-2").addClass('material-button');
            }

        });

    });
</script>
<script src="<?php echo base_url() ?>assets/js/bootstrap-notify.js"></script>
<script src="<?php echo base_url() ?>assets/js/demo.js"></script>

</div>
</div>
</body>
</html>