<div class="background"></div>
<div class="background2"></div>
<div class="loginForm">
    <hgroup>
        <h1>Forgot Password</h1>
    </hgroup>
    <form action="<?php echo base_url(); ?>Forgot_password/verify_details" method="post">
        <div class="form-group label-floating">
            <label class="control-label" >Username / Email</label>
            <input type="text" name="username" id="username" class="form-control" required oninvalid="setCustomValidity('Please enter username or email ID')"
                   onchange="try{setCustomValidity('')}catch(e){}">
        </div>
        <button type="submit" class="btn btn-primary"><span> Send Temporary Password </span>
            <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
        </button>
    </form>
    <h4 style="color: rgba(0, 0, 0, 0.61);">Don't have account yet? Create one now by clicking below</h4>
    <a href="<?php echo base_url(); ?>AuthCtrl/register">
        <button type="submit" class="btn btn-primary"><span> Register </span>
            <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
        </button>
    </a>
    <br/><br/>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>$(window, document, undefined).ready(function () {

        $('input').blur(function () {
            var $this = $(this);
            if ($this.val())
                $this.addClass('used');
            else
                $this.removeClass('used');
        });

        var $ripples = $('.ripples');

        $ripples.on('click.Ripples', function (e) {

            var $this = $(this);
            var $offset = $this.parent().offset();
            var $circle = $this.find('.ripplesCircle');

            var x = e.pageX - $offset.left;
            var y = e.pageY - $offset.top;

            $circle.css({
                top: y + 'px',
                left: x + 'px'
            });

            $this.addClass('is-active');

        });

        $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
            $(this).removeClass('is-active');
        });

    });

</script>

