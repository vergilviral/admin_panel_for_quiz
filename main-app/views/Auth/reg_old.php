<div class="background"></div>
<div class="background2"></div>
<div class="loginForm">
	<hgroup>
		<h1>Register</h1>
	</hgroup>
	<form action="<?php echo base_url(); ?>AuthCtrl/new_user" method="post">
		<div class="group">
			<input type="text" name="username"><span class="highlight"></span><span class="bar"></span>
			<label>Username</label>
		</div>
		<div class="form-group label-floating">
			<label class="control-label" >Username</label>
			<input type="text" name="username" id="username" class="form-control" required oninvalid="setCustomValidity('Please Enter Username.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<div class="form-group label-floating">
			<label class="control-label">First Name</label>
			<input type="text" name="first_name" id="first_name" class="form-control" required oninvalid="setCustomValidity('Please Enter First Name.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<div class="form-group label-floating">
			<label class="control-label" >Last Name</label>
			<input type="text" name="last_name" id="last_name" class="form-control">
		</div>
		<div class="form-group label-floating">
			<label class="control-label">Institute Name</label>
			<input type="text" name="institute_name" id="institute_name" value="UTS" class="form-control" required oninvalid="setCustomValidity('Please Enter Your Institute / Institute You Work In.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<div class="form-group label-floating">
			<label class="control-label" >Email</label>
			<input type="email" name="email" id="email" class="form-control" required oninvalid="setCustomValidity('Please Enter Valid Email.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<div class="form-group label-floating">
			<label class="control-label" >Password</label>
			<input type="password" name="password" id="password" class="form-control" required oninvalid="setCustomValidity('Please Enter Password.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<div class="form-group label-floating">
			<label class="control-label" >Confirm Password</label>
			<input type="password" name="confirm_pass" id="confirm_pass" class="form-control" required oninput="check(this)">
		</div>
        <script language='javascript' type='text/javascript'>
            function check(input) {
                if (input.value != document.getElementById('password').value) {
                    input.setCustomValidity('Passwords Do Not Match. Please Enter Same Password in Password and Confirm Password Field.');
                } else {
                    input.setCustomValidity('');
                }
            }
        </script>
		<button type="submit" class="btn btn-primary "><span> Register </span>
			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
		</button>
	</form>
	<h4 style="color: rgba(0, 0, 0, 0.61);">Already Have an Account? Log in by clicking below</h4>
	<a href="<?php echo base_url(); ?>AuthCtrl">
		<button type="submit" class="btn btn-primary"><span> Login </span>
			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
		</button>
	</a>
	<br/><br/>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>$(window, document, undefined).ready(function () {

		$('input').blur(function () {
			var $this = $(this);
			if ($this.val())
				$this.addClass('used');
			else
				$this.removeClass('used');
		});

		var $ripples = $('.ripples');

		$ripples.on('click.Ripples', function (e) {

			var $this = $(this);
			var $offset = $this.parent().offset();
			var $circle = $this.find('.ripplesCircle');

			var x = e.pageX - $offset.left;
			var y = e.pageY - $offset.top;

			$circle.css({
				top: y + 'px',
				left: x + 'px'
			});

			$this.addClass('is-active');

		});

		$ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
			$(this).removeClass('is-active');
		});

	});

</script>

<div class="background"></div>
<div class="background2"></div>
<div class="loginForm">
	<hgroup>
		<h1>Login</h1>
	</hgroup>
	<form action="<?php echo base_url(); ?>AuthCtrl/login_verify" method="post">
		<div class="form-group label-floating">
			<label class="control-label" >Username</label>
			<input type="text" name="username" id="username" class="form-control" required oninvalid="setCustomValidity('Please enter username')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>

		<div class="form-group label-floating">
			<label class="control-label">Password</label>
			<input type="password" name="password" id="password" class="form-control" required oninvalid="setCustomValidity('Please enter password.')"
                   onchange="try{setCustomValidity('')}catch(e){}">
		</div>
		<button type="submit" class="btn btn-primary"><span> Login </span>
			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
		</button>
	</form>
	<h4 style="color: rgba(0, 0, 0, 0.61);">Don't have account yet? Create one now by clicking below</h4>
	<a href="<?php echo base_url(); ?>AuthCtrl/register">
	<button type="submit" class="btn btn-primary"><span> Register </span>
		<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
	</button>
	</a>
	<br/><br/>
    <h4 style="color: rgba(0, 0, 0, 0.61);">Forgot your password? No worries, recover it by clicking below</h4>
    <a href="<?php echo base_url(); ?>Forgot_password">
        <button type="submit" class="btn btn-primary"><span> Forgot Password </span>
            <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
        </button>
    </a>
    <br/><br/>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>$(window, document, undefined).ready(function () {

		$('input').blur(function () {
			var $this = $(this);
			if ($this.val())
				$this.addClass('used');
			else
				$this.removeClass('used');
		});

		var $ripples = $('.ripples');

		$ripples.on('click.Ripples', function (e) {

			var $this = $(this);
			var $offset = $this.parent().offset();
			var $circle = $this.find('.ripplesCircle');

			var x = e.pageX - $offset.left;
			var y = e.pageY - $offset.top;

			$circle.css({
				top: y + 'px',
				left: x + 'px'
			});

			$this.addClass('is-active');

		});

		$ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
			$(this).removeClass('is-active');
		});

	});

</script>

