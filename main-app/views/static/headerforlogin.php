<!doctype html>
<html lang="en">

<head>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <!-- add all favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/favicons//apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/favicons//apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/favicons//apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/favicons//apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/favicons//apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/favicons//apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/favicons//apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/favicons//apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url() ?>assets/favicons//android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/favicons//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/favicons//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/favicons//favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/favicons//manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/favicons//ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- end of favicons -->

    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans|PT+Sans|Roboto|Roboto+Slab|Titillium+Web"
          rel="stylesheet">
    <style class="cp-pen-styles">
        html, body {
            background: white !important;
        }
        .box {
            position: relative;
            top: 0;
            opacity: 1;
            float: left;
            padding: 60px 50px 40px 50px;
            width: 100%;
            background: #fff;
            border-radius: 10px;
            transform: scale(1);
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            z-index: 5;
        }

        .box.back {
            transform: scale(.95);
            -webkit-transform: scale(.95);
            -ms-transform: scale(.95);
            top: -20px;
            opacity: .8;
            z-index: -1;
        }

        .box:before {
            content: "";
            width: 100%;
            height: 30px;
            border-radius: 10px;
            position: absolute;
            top: -10px;
            background: rgba(255, 255, 255, .6);
            left: 0;
            transform: scale(.95);
            -webkit-transform: scale(.95);
            -ms-transform: scale(.95);
            z-index: -1;
        }

        .overbox .title {
            color: #fff;
        }

        .overbox .title:before {
            background: #fff;
        }

        .title {
            width: 100%;
            float: left;
            line-height: 46px;
            font-size: 34px;
            font-weight: 700;
            letter-spacing: 2px;
            color: #ED2553;
            position: relative;
        }

        .title:before {
            content: "";
            width: 5px;
            height: 100%;
            position: absolute;
            top: 0;
            left: -50px;
            background: #ED2553;
        }

        .input,
        .input label,
        .input input,
        .input .spin,
        .button,
        .button button .button.login button i.fa,
        .material-button .shape:before,
        .material-button .shape:after,
        .button.login button {
            transition: 300ms cubic-bezier(.4, 0, .2, 1);
            -webkit-transition: 300ms cubic-bezier(.4, 0, .2, 1);
            -ms-transition: 300ms cubic-bezier(.4, 0, .2, 1);
        }

        .material-button,
        .alt-2,
        .material-button .shape,
        .alt-2 .shape,
        .box {
            transition: 400ms cubic-bezier(.4, 0, .2, 1);
            -webkit-transition: 400ms cubic-bezier(.4, 0, .2, 1);
            -ms-transition: 400ms cubic-bezier(.4, 0, .2, 1);
        }

        .input,
        .input label,
        .input input,
        .input .spin,
        .button,
        .button button {
            width: 100%;
            float: left;
        }

        .input,
        .button {
            margin-top: 30px;
            height: 70px;
        }

        .input,
        .input input,
        .button,
        .button button {
            position: relative;
        }

        .input input {
            height: 60px;
            top: 10px;
            border: none;
            background: transparent;
        }

        .input input,
        .input label,
        .button button {
            font-family: 'Roboto', sans-serif;
            font-size: 24px;
            color: rgba(0, 0, 0, 0.8);
            font-weight: 300;
        }

        .input:before,
        .input .spin {
            width: 100%;
            height: 1px;
            position: absolute;
            bottom: 0;
            left: 0;
        }

        .input:before {
            content: "";
            background: rgba(0, 0, 0, 0.1);
            z-index: 3;
        }

        .input .spin {
            background: #ED2553;
            z-index: 4;
            width: 0;
        }

        .overbox .input .spin {
            background: rgba(255, 255, 255, 1);
        }

        .overbox .input:before {
            background: rgba(255, 255, 255, 0.5);
        }

        .input label {
            position: absolute;
            top: 10px;
            left: 0;
            z-index: 2;
            cursor: pointer;
            line-height: 60px;
        }

        .button.login {
            width: 60%;
            left: 20%;
        }

        .button.login button,
        .button button {
            width: 100%;
            line-height: 64px;
            left: 0%;
            background-color: transparent;
            border: 3px solid rgba(0, 0, 0, 0.1);
            font-weight: 900;
            font-size: 18px;
            color: rgba(0, 0, 0, 0.2);
        }

        .button.login {
            margin-top: 30px;
        }

        .button {
            margin-top: 20px;
        }

        .button button {
            background-color: #fff;
            color: #ED2553;
            border: none;
        }

        .button.login button.active {
            border: 3px solid transparent;
            color: #fff !important;
        }

        .button.login button.active span {
            opacity: 0;
            transform: scale(0);
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
        }

        .button.login button.active i.fa {
            opacity: 1;
            transform: scale(1) rotate(-0deg);
            -webkit-transform: scale(1) rotate(-0deg);
            -ms-transform: scale(1) rotate(-0deg);
        }

        .button.login button i.fa {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            line-height: 60px;
            transform: scale(0) rotate(-45deg);
            -webkit-transform: scale(0) rotate(-45deg);
            -ms-transform: scale(0) rotate(-45deg);
        }

        .button.login button:hover {
            color: #ED2553;
            border-color: #ED2553;
        }

        .button {
            margin: 40px 0;
            overflow: hidden;
            z-index: 2;
        }

        .button button {
            cursor: pointer;
            position: relative;
            z-index: 2;
        }

        .pass-forgot {
            width: 100%;
            float: left;
            text-align: center;
            color: rgba(0, 0, 0, 0.4);
            font-size: 18px;
        }

        .click-efect {
            position: absolute;
            top: 0;
            left: 0;
            background: #ED2553;
            border-radius: 50%;
        }

        .overbox {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            overflow: inherit;
            border-radius: 10px;
            padding: 60px 50px 40px 50px;
        }

        .overbox .title,
        .overbox .button,
        .overbox .input {
            z-index: 111;
            position: relative;
            color: #fff !important;
            display: none;
        }

        .overbox .title {
            width: 80%;
        }

        .overbox .input {
            margin-top: 20px;
        }

        .overbox .input input,
        .overbox .input label {
            color: #fff;
        }

        .overbox .material-button,
        .overbox .material-button .shape,
        .overbox .alt-2,
        .overbox .alt-2 .shape {
            display: block;
        }

        .material-button,
        .alt-2 {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            background: #ED2553;
            position: absolute;
            top: 40px;
            right: -70px;
            cursor: pointer;
            z-index: 100;
            transform: translate(0%, 0%);
            -webkit-transform: translate(0%, 0%);
            -ms-transform: translate(0%, 0%);
        }

        .material-button .shape,
        .alt-2 .shape {
            position: absolute;
            top: 0;
            right: 0;
            width: 100%;
            height: 100%;
        }

        .material-button .shape:before,
        .alt-2 .shape:before,
        .material-button .shape:after,
        .alt-2 .shape:after {
            content: "";
            background: #fff;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) rotate(360deg);
            -webkit-transform: translate(-50%, -50%) rotate(360deg);
            -ms-transform: translate(-50%, -50%) rotate(360deg);
        }

        .material-button .shape:before,
        .alt-2 .shape:before {
            width: 25px;
            height: 4px;
        }

        .material-button .shape:after,
        .alt-2 .shape:after {
            height: 25px;
            width: 4px;
        }

        .material-button.active,
        .alt-2.active {
            top: 50%;
            right: 50%;
            transform: translate(50%, -50%) rotate(0deg);
            -webkit-transform: translate(50%, -50%) rotate(0deg);
            -ms-transform: translate(50%, -50%) rotate(0deg);
        }

        body {
            min-height: 100vh;
            font-family: 'Roboto', sans-serif;
        }

        body,
        html {
            /*overflow: hidden;*/
        }

        .materialContainer {
            width: 100%;
            max-width: 460px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
        }

        .materialContainer.reg {
            max-width: 700px;
        }

        @media screen and (max-width: 995px){
            .materialContainer.reg{
                /*max-width: 900px;*/
                top: 0;
                left: 0;
                transform: unset;
                -webkit-transform: unset;
                -ms-transform: unset;

            }
        }

        *,
        *:after,
        *::before {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            text-decoration: none;
            list-style-type: none;
            outline: none;
        }
    </style>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>VedaQuiz - Admin Panel</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
	<link href="<?php echo base_url() ?>assets/css/demo.css" rel="stylesheet" />
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" />

    <style>
		::-webkit-input-placeholder { text-align:right; }
		/* mozilla solution */
		input:-moz-placeholder { text-align:right; }
	</style>

</head>

<body>
<?php
if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg . "', 'success');};</script>";
}
if(isset($error)){
//    echo $error;
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error . "', 'danger');};</script>";
}
if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message . "', 'success');};</script>";
}


?>
