<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Admin Panel</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
<style>
    .contentdiv {
        color: white;
        font-family: "Roboto", "Helvetica", "Arial", sans-serif;
        width: 70%;
        padding: 50px;
        padding-bottom: 30px;
        padding-top: 0px;
        text-align: center;line-height: 50px;
    }
    .homelink{
        text-decoration: none;
        color: white;
        font-weight: 400;
        background: rgba(255, 152, 0, 0.65);
        padding: 15px;
        border-radius: 5px;
    }
    body{
        height: 100vh; 
        width: 100vw; 
        background: url('<?php echo base_url();?>assets/ops.jpg');
    }
    
    h3 {
        font-size: 36px;
        margin-top: 10px; margin-bottom: 10px;
    }
    
    .container {
        padding-top: 50px;
    }
    
    @media screen and (max-width: 768px){
        .container {
            background: rgba(0,0,0,0.5);
            margin: 0 auto;
            padding-top: 40px;
            padding-bottom: 20px;
            height: 100vh;
            width: 100vw;
        }
        body {
            margin: 0;
            background: url('<?php echo base_url();?>assets/port.png');
            background-position: center center;
        }
        h3 {
            margin: 0;
            font-size: 18px;
        }
    }
</style>
</head>
 <!---->
<body >
    <div class="container">
        <div class="contentdiv"><h3>I am a junior developer and you have landed on my homepage. </h3></div>
        <div class="contentdiv"><h3>Usually the lost souls land here!</h3></div>
        <div class="contentdiv"><h4><a href="http://quizadmin.veyasoftsolutions.com/home" class="homelink">GO TO HOME NOW</a></h4></div>
    </div>
</body>


</html>
