<footer class="footer">
	<div class="container-fluid">
<!--		<nav class="pull-left">-->
<!--			<ul>-->
<!--				<li>-->
<!--					<a href="#">-->
<!--						Home-->
<!--					</a>-->
<!--				</li>-->
<!--				<li>-->
<!--					<a href="#">-->
<!--						Company-->
<!--					</a>-->
<!--				</li>-->
<!--				<li>-->
<!--					<a href="#">-->
<!--						Portfolio-->
<!--					</a>-->
<!--				</li>-->
<!--				<li>-->
<!--					<a href="#">-->
<!--						Blog-->
<!--					</a>-->
<!--				</li>-->
<!--			</ul>-->
<!--		</nav>-->
		<p class="copyright pull-right">
			&copy;
			<script>
				document.write(new Date().getFullYear())
			</script>
			<a href="http://vedaquiz.com/home" target="_blank">VedaQuiz</a>, made with love for better classrooms
		</p>
	</div>
</footer>
</div>
</div>
</body>
<!--   Core JS Files   -->
<script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url() ?>assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="<?php echo base_url() ?>assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="<?php echo base_url() ?>assets/js/perfect-scrollbar.jquery.min.js"></script>

<!-- Extras by Viral -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>


<!--  Notifications Plugin    -->
<script src="<?php echo base_url() ?>assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>-->
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url() ?>assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url() ?>assets/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		// Javascript method's body can be found in assets/js/demos.js
		demo.initDashboardPageCharts();
        $( function() {
            $( "#sortable" ).sortable({
                update: function(event, ui) {
                    console.log('update: '+ui.item.index());
                    console.log('id: '+ui.item.attr('id'))
                },
                start: function(event, ui) {
                    console.log('start: ' + ui.item.index());
                    console.log('id: '+ui.item.attr('id'));
                },
                stop: function(event, ui) {
                    $("#mainorder").val($.map($(this).find('li'), function(el) {
                        return el.id;
                    }));
//                    console.log($.map($(this).find('li'), function(el) {
//                        return el.id;
//                    }));
                    console.log($("#mainorder").val());
                }
            });
            $( "#sortable" ).disableSelection();
        } );
	});
</script>
</html>
