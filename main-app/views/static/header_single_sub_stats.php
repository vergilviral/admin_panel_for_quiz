<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/apple-icon.png" />
    <!-- add all favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/favicons//apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/favicons//apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/favicons//apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/favicons//apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/favicons//apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/favicons//apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/favicons//apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/favicons//apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url() ?>assets/favicons//android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/favicons//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/favicons//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/favicons//favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/favicons//manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/favicons//ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- end of favicons -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>VedaQuiz - Admin Panel</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        ::-webkit-input-placeholder { text-align:right; }
        /* mozilla solution */
        input:-moz-placeholder { text-align:right; }
    </style>

</head>
<!---->
<body onload="initAllChartsSingleSub();" onmouseenter="initAllCharts()">
<?php

if(!isset($_SESSION['user_name'])) {
    header("Location: " . base_url());
    exit;
}

?>
<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="<?php echo base_url(); ?>assets/img/sidebar-1.jpg">
        <div class="logo">
            <a href="<?php echo base_url(); ?>home" class="simple-text">
                <img class="img-fluid computer brand" src="<?php echo base_url() ?>/assets/img/vqlogodark.png" />
                <img class="img-fluid mobile brand" src="<?php echo base_url() ?>/assets/img/vqlogomobdark.png" />
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active">
                    <a href="<?php echo base_url(); ?>home">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a data-toggle="collapse" href="#manageQuiz" class="collapsed" aria-expanded="false">
                        <i class="material-icons">question_answer</i>
                        <p> Manage Quiz
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="manageQuiz" aria-expanded="false" style="height: 147px;">
                        <ul class="nav">
                            <li>
                                <a href="<?php echo base_url(); ?>Add_quiz">
                                    <i class="material-icons">add</i>
                                    <p>Add</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Edit_quiz">
                                    <i class="material-icons">create</i>
                                    <p>Edit</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Delete_quiz">
                                    <i class="material-icons">remove</i>
                                    <p>Delete</p>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
                <li>
                    <a data-toggle="collapse" href="#manageQuestions" class="collapsed" aria-expanded="false">
                        <i class="material-icons">help</i>
                        <p> Manage Questions
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="manageQuestions" aria-expanded="false" style="height: 147px;">
                        <ul class="nav">
                            <li>
                                <a href="<?php echo base_url(); ?>Add_questions">
                                    <i class="material-icons">add</i>
                                    <p>Add</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Edit_questions">
                                    <i class="material-icons">create</i>
                                    <p>Edit</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Order_questions">
                                    <i class="material-icons">cached</i>
                                    <p>Re-order</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>Delete_question">
                                    <i class="material-icons">remove</i>
                                    <p>Delete</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>Single_statistics">
                        <i class="material-icons">graphic_eq</i>
                        <p>Single Subject Statistics</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>Get_links">
                        <i class="material-icons">link</i>
                        <p>Get Sharable Link of Quiz</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>Get_single_quiz_results">
                        <i class="material-icons text-gray">file_download</i>
                        <p>Get Single Quiz Results</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>Edit_profile">
                        <i class="material-icons">insert_emoticon</i>
                        <p>Edit Profile</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>AuthCtrl/logout">
                        <i class="material-icons">lock</i>
                        <p>Log Out</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>feedback">
                        <i class="material-icons">comment</i>
                        <p>Leave Feedback</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> Admin Panel </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <img src="<?= base_url() ?>uploads/<?= $_SESSION['profile_pic'] ?>" style="height: 50px; width: 50px; border-radius: 100%;" />
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo base_url(); ?>Edit_profile">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>AuthCtrl/logout">Log Out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
