<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Select Subject</h4>
                        <p class="category">Select subject from below to see statistics of quizzes related to that subject.</p>
                    </div>
                    <div class="card-content">
                        <form action="<?php echo base_url(); ?>Single_statistics/show_stats" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Select Quiz To Edit</p>
                                    <select id="sub_names_avail" name="sub_names_avail" class="selectpicker" data-style="btn-info" required oninvalid="setCustomValidity('Oops. Something Seems Fishy. Please Go To Dashboard And Come Back.')"
                                            onchange="try{setCustomValidity('')}catch(e){}">
                                        <?php
                                        $avail_subjects_name = array();

                                        if(!empty($avail_subjects))
                                            foreach ($avail_subjects as $row)
                                            {
                                                if(isset($subject_name)) {
                                                    if ($row['subject_name'] == $subject_name) {
                                                        echo '<option value="' . $row['subject_name'] . '" selected>' . $row['subject_name'] . '</option>';
                                                    }
                                                }
                                                else{
                                                    echo '<option value="' . $row['subject_name'] . '">' . $row['subject_name'] . '</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <a href="<?php echo base_url(); ?>index.php/home">
                                <button type="button" class="btn btn-primary pull-left">Go Back</button>
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">Show Statistics</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
