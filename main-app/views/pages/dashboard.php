<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="blue">
						<i class="material-icons">note_add</i>
					</div>
					<div class="card-content">
						<p class="category">Total Quizzes Created</p>
						<?php
						$total_quiz = 0;
						foreach ($quiz_info as $row)
						{
							$total_quiz = $total_quiz + 1;
						}
						echo '<h3 class="title">'. $total_quiz .'</h3>';
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="card card-stats">
					<div class="card-header" data-background-color="red">
						<i class="material-icons">account_circle</i>
					</div>
					<div class="card-content">
						<p class="category">Total Students Participated</p>
						<?php
						$total_students = 0;
						foreach ($quiz_info as $row)
						{
							$total_students = $total_students + $row['total_students_joined'];
						}
						echo '<h3 class="title">'. $total_students .'</h3>';
						?>
                    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-chart" data-background-color="green">
						<div class="ct-chart" id="totalStudents"></div>
					</div>
					<div class="card-content">
						<h4 class="title">Students Joined </h4>
						<p class="category">
							Per Quiz
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-chart" data-background-color="orange">
						<div class="ct-chart" id="totalRights"></div>
					</div>
					<div class="card-content">
						<h4 class="title">% of Total Right Answers Given</h4>
						<p class="category">Per Quiz (Considering total students joined, total questions and total right answers)</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
    function initAllCharts() {
        dataTotalStudentsJoined = {
            labels: [
                <?php foreach ($quiz_info as $row)
            {
                echo '\'' . $row['quiz_name'] . '\',';
            } ?>],
            series: [
                [
                    <?php foreach ($quiz_info as $row)
                {
                    echo '\'' . $row['total_students_joined'] . '\',';
                } ?>
                ]
            ]
        };

        optionsTotalStudentsJoined = {
            height: 200,
            axisX: {
                showGrid: false,
                onlyInteger: true
            },
            axisY: {
                onlyInteger: true
            },
            low: 0,
            high: <?php $temp = 0; foreach ($quiz_info as $row)

            {
            if($row['total_students_joined'] > $temp)
                $temp = $row['total_students_joined'];
            }
            if($temp != 0)
                echo $temp*2;
            else
                echo '50';
            ?>,
            chartPadding: {
                top: 0,
                right: 5,
                bottom: 0,
                left: 0
            }
        }

        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 15,
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ];


        var totalStudents = Chartist.Bar('#totalStudents', dataTotalStudentsJoined, optionsTotalStudentsJoined, responsiveOptions);

        md.startAnimationForBarChart(totalStudents);

        var dataTotalRights = {
            labels: [<?php foreach ($quiz_info as $row)
            {
                echo '\'' . $row['quiz_name'] . '\',';
            } ?>],
            series: [
                [
                    <?php foreach ($quiz_info as $row)
                {
                    // if($row['total_students_joined']*$row['total_questions'] == 0){
                    //     echo '\'0\',';
                    // }
                    // else
                    if($row['total_students_joined']*$row['total_questions'] > 0) {
                        echo '\'' . ($row['total_rights'] * 100 / ($row['total_students_joined'] * $row['total_questions'])) . '\',';
                    }
                    else {
                        echo '\'0\',';
                    }
                } ?>
                ]

            ]
        };
        var optionsTotalRights = {
            height: 200,
            axisX: {
                showGrid: false,
                onlyInteger: true
            },
            axisY: {
                onlyInteger: true
            },
            low: 0,
            high: 120,
            chartPadding: {
                top: 0,
                right: 5,
                bottom: 0,
                left: 0
            }
        };
        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 15,
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ];
        var totalRights = Chartist.Bar('#totalRights', dataTotalRights, optionsTotalRights, responsiveOptions);

        md.startAnimationForBarChart(totalRights);
    }
</script>