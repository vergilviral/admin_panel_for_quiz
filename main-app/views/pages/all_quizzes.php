<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?><div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Get Sharable Links</h4>
						<p class="category">Click on 'Copy Link' button for any quiz to copy link in your clipboard. You can also share the link given in center. This link is used to join quiz.</p>
					</div>
					<div class="card-content table-responsive">
<!--						<script>-->
<!--							function copyLink(id) {-->
<!--								var copyText = document.getElementById(id);-->
<!--								copyText.select();-->
<!--								document.execCommand("Copy");-->
<!--								alert("Copied the link: " + copyText.value);-->
<!--							}-->
<!--						</script>-->

						<table class="table">
							<tbody class="centertext">
                            <tr>
                                <th>Quiz Name</th>
                                <th>Quiz ID</th>
                                <th>Sharable Link</th>
                                <th>Copy Link</th>
                            </tr>

							<?php
							$avail_quiz_names = array();

							if(!empty($avail_quizzes))
								foreach ($avail_quizzes as $row)
								{
									echo '<tr>';
									echo '<td>' . $row['title'] . '</td>';
									echo '<td>' . $row['keyword'] . '</td>';
									echo '<td><div class="form-group label-floating"><label class="control-label">Sharable Link</label><input class="form-control" disabled type="text" value="http://qveda.co/'. $row['keyword'] .'" id="'. $row['keyword'] .'"></input></div></td>';
//									echo '<td><button type="submit" class="btn btn-primary" id="'. $row['title'] .'" onclick="copyLink(\''. $row['keyword'] .'\')">Copy Link</button></td>';
									echo '<td><button type="submit" class="copylink btn btn-primary" id="'. $row['title'] .'" data-clipboard-text="http://qveda.co/'. $row['keyword'] .'">Copy Link</button></td>';
									echo '</tr>';
								}
							?>
							</tbody>
							<tr>
								<td>
                                    <a href="<?php echo base_url(); ?>index.php/home">
                                        <button type="button" class="btn btn-primary pull-left">Go Back</button>
                                    </a>
                                </td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var clipboard = new ClipboardJS('.copylink');

    clipboard.on('success', function(e) {
        alert('Successfully copied the link to your clipboard');
        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        alert('Oops. Something went wrong. Please try again or try copying link from sharble link box.');
    });

</script>