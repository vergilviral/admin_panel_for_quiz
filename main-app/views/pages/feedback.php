<?php

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','Your question has been successfully added.', 'success');};</script>";
}

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error . "', 'danger');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Leave Feedback</h4>
                        <p class="category">Good or bad, we'd love to know what you think about us.</p>
                    </div>
                    <div class="card-content">
                        <form onsubmit="return confirm('Are you sure you want to submit this feedback?');" action="<?php echo base_url(); ?>Feedback/submit_feedback" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="feedback">Enter Your Feedback</label>
                                        <textarea class="form-control" id="feedback" rows="6" name="feedback" required></textarea>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="agreement" name="agreement" value="1" checked>
                                        <label class="form-check-label" for="agreement">Do you mind if we contact you regarding this feedback?</label>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url(); ?>index.php/home">
                                <button type="button" class="btn btn-primary pull-left">Go Back</button>
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">Submit Feedback</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload =  function(){
        $("#userfile").fileinput({'showUpload':false});
    };
</script>
