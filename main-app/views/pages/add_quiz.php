<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Add Quiz</h4>
						<p class="category">Please Fill The Info Below to Add Quiz. This details will not be displayed while taking quiz. This is only for your information.</p>
					</div>
					<div class="card-content">
						<form onsubmit="return confirm('Are you sure you want to add quiz with given details? These details can not be changed later.');" action="<?php echo base_url(); ?>Add_quiz/add_new" method="post">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Quiz Name</label>
										<input type="text" class="form-control" id="quiz_name" name="quiz_name" required oninvalid="setCustomValidity('Please Enter The Quiz Name.')"
                                               onchange="fillUniqueID();" onkeyup="fillUniqueID();">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Subject Name</label>
										<input type="text" class="form-control" id="sub_name" name="sub_name" required oninvalid="setCustomValidity('Please Enter The Subject Name.')"
                                               onchange="fillUniqueID();" onkeyup="fillUniqueID();">
									</div>
								</div>
							</div>
                            <div class="row">
                                <div class="col-md-12"><br/>
                                    <label style="margin-right: 15px;">Quiz Type</label><br/>
                                    <select id="quiz_type" name="quiz_type" class="selectpicker" data-style="btn-info" required oninvalid="setCustomValidity('Oops. Something Seems Fishy. Please Go To Dashboard And Come Back.')"
                                            onchange="checkType();">
                                        <option value="Normal" selected>Normal</option>
                                        <option value="Traverse">Traverse</option>
                                        <option value="FixedTime">Fixed Time</option>
                                    </select>
                                    <p id="quizt"></p>
                                </div>
                            </div>
                            <div class="row" id="timingsfixed">
                                <label style="padding-left: 15px; padding-top:10px;">Time Limit</label>
                                <div class="col-md-12">
                                    <div class="col-md-4 timingsquiz">
                                        <input type="Number" class="form-control" id="hrs" name="hrs" oninvalid="setCustomValidity('Please Enter Proper Hours.')"
                                               onchange="try{setCustomValidity('')}catch(e){}"> Hrs
                                    </div>
                                    <div class="col-md-4 timingsquiz">
                                        <input type="Number" class="form-control" id="mins" name="mins" oninvalid="setCustomValidity('Please Enter Proper Minutes.')"
                                               onchange="try{setCustomValidity('')}catch(e){}"> Min
                                    </div>
                                    <div class="col-md-4 timingsquiz">
                                        <input type="Number" class="form-control" id="secs" name="secs" oninvalid="setCustomValidity('Please Enter Proper Seconds.')"
                                               onchange="try{setCustomValidity('')}catch(e){}"> Secs
                                    </div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Unique ID (this will be used in your URL to identify quiz)</label>
										<input type="text" class="form-control" id="unique_id" name="unique_id" required oninvalid="setCustomValidity('Please Enter The Unique ID.')"
                                               onchange="try{setCustomValidity('')}catch(e){}">
									</div>
								</div>
							</div>
							<a href="<?php echo base_url(); ?>index.php/home">
								<button type="button" class="btn btn-primary pull-left">Go Back</button>
							</a>
                            <button type="submit" class="btn btn-primary pull-right">Add Quiz</button>

							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    window.onload = function() {
        checkType();
        fillUniqueID();
//        var option = document.getElementById('quiz_type');
//        var selected = option.options[option.selectedIndex].text;
//
//        if(selected == 'Normal'){
//            $("#quizt").text('This quiz mode allows lecturer to control the quiz. With this mode, lecturer go to next question of the quiz but doesn\'n have option to go back in the quiz.');
//        }
//        if(selected == 'Traverse'){
//            $("#quizt").text('This quiz mode has all features of normal quiz mode, but it allows the lecturer to go back and forth and jump to any question at any point in the quiz.');
//        }
//
//        if(selected == 'Fixed Time'){
//            $("#quizt").text('With this quiz mode, the lecturer can assign the time limit to quiz and student will have to finish the quiz by that time. Full control of quiz will be to the students and lecturer will be able to see live statistics like no. of correct/incorrect/partially correct answers given and much more.');
//        }
    }
    function checkType(){
        var option = document.getElementById('quiz_type');
        var otherblock = document.getElementById('timingsfixed');
        var selected = option.options[option.selectedIndex].text;

        if(selected == 'Normal'){
            $("#quizt").text('This quiz mode allows lecturer to control the quiz. With this mode, lecturer go to next question of the quiz but doesn\'n have option to go back in the quiz.');
        }
        if(selected == 'Traverse'){
            $("#quizt").text('This quiz mode has all features of normal quiz mode, but it allows the lecturer to go back and forth and jump to any question at any point in the quiz.');
        }

        if(selected == 'Fixed Time'){
            $("#quizt").text('With this quiz mode, the lecturer can assign the time limit to quiz and student will have to finish the quiz by that time. Full control of quiz will be to the students and lecturer will be able to see live statistics like no. of correct/incorrect/partially correct answers given and much more.');
            otherblock.style.display = ('block');
        }
        else{
            otherblock.style.display = ('none');
        }
    }

    function fillUniqueID(){
        try{setCustomValidity('')}catch(e){};
        var name = $("#quiz_name").val().replace(/\s/g, "").replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var sname = $("#sub_name").val().replace(/\s/g, "").replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        if(name != '' && sname != ''){
            $("#unique_id").val($.trim(name)+$.trim(sname));
        }
        else {
            $("#unique_id").val('Please enter valid quiz name and subject name');
        }

    }
</script>