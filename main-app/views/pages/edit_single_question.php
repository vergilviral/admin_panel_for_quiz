<?php

if(isset($message)){
	echo "<script>window.onload = function() {demo.showNotification('top','center','Your question has been successfully updated.', 'success');};</script>";

}
?>

<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Update Question</h4>
						<p class="category">Edit details of the question below and click 'Done' to update the question / answers.</p>
					</div>
					<div class="card-content">
						<form onsubmit="return confirm('Are you sure you want to update question with given details? These details can be changed later from \'Edit Questions\' option.');" action="<?php echo base_url(); ?>Edit_questions/update_single_question" method="post" enctype="multipart/form-data">

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Question</label>
										<input type="text" class="form-control" id="que" name="que" value="<?php echo $selected_question ?>" required oninvalid="setCustomValidity('Please Enter Question Name For This Question.')"
                                               onchange="try{setCustomValidity('')}catch(e){}">
									</div>
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="partial">
                                        <input type="checkbox" id="partial" name="partial" class="mdl-checkbox__input" value="1" <?php if($single_question_details['partial_marking']==1){echo 'checked';} ?>>
                                        <span class="mdl-checkbox__label">Allow partial marking for this question?</span>
                                    </label>
								</div>
							</div>
                            <br/>
                            <p>Add Answers Below</p>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 1</label>
										<input type="text" class="form-control" id="ans1" name="ans1" value="<?php echo $single_question_details['answers'][0]['answer'] ?>" required oninvalid="setCustomValidity('At Least 1 Answer For This Question Must Be Set.')"
                                               onchange="try{setCustomValidity('')}catch(e){}">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check1">
										<input type="checkbox" id="check1" name="check1" class="mdl-checkbox__input" value="1" <?php if($single_question_details['answers'][0]['is_true']==1){echo 'checked';} ?>>
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 2</label>
										<input type="text" class="form-control" id="ans2" name="ans2" value="<?php echo $single_question_details['answers'][1]['answer'] ?>">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check2">
										<input type="checkbox" id="check2" name="check2" class="mdl-checkbox__input" value="1" <?php if($single_question_details['answers'][1]['is_true']==1){echo 'checked';} ?>>
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 3</label>
										<input type="text" class="form-control" id="ans3" name="ans3" value="<?php echo $single_question_details['answers'][2]['answer'] ?>">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check3">
										<input type="checkbox" id="check3" name="check3" class="mdl-checkbox__input" value="1" <?php if($single_question_details['answers'][2]['is_true']==1){echo 'checked';} ?>>
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 4</label>
										<input type="text" class="form-control" id="ans4" name="ans4" value="<?php echo $single_question_details['answers'][3]['answer'] ?>">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check4">
										<input type="checkbox" id="check4" name="check4" class="mdl-checkbox__input" value="1" <?php if($single_question_details['answers'][3]['is_true']==1){echo 'checked';} ?>>
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
                            <div class="row">
                                <div class="col-md-12">
                                    Upload image to show in question:<br /><br/>
                                    Current set image:<br/><br/>
                                    <img style="height: 200px;width: auto;" class="img-thumbnail" src="<?= base_url() ?>uploads/<?= $img_path ?>"><br/><br/>
                                    <input type="file" id="userfile" name="userfile"/><br/>
                                </div>
                            </div>
							<a href="<?php echo base_url(); ?>index.php/home">
								<button type="button" class="btn btn-primary pull-left">Go Back</button>
							</a>
                            <button type="submit" class="btn btn-primary pull-right">Done</button>

                            <div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    window.onload =  function(){
        $("#userfile").fileinput({'showUpload':false});
    };
</script>
