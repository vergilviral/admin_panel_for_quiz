<?php

if(isset($message)){
	echo "<script>window.onload = function() {demo.showNotification('top','center','Your question has been successfully added.', 'success');};</script>";

}
?>

<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Add Questions</h4>
						<p class="category">Add questions to quiz from here</p>
					</div>
					<div class="card-content">
						<form action="<?php echo base_url(); ?>Add_questions/add_new_question" method="post">
							<div class="row">
								<div class="col-md-12">
									<p>Select Quiz To Edit</p>
									<select id="quiz_names_avail" name="quiz_names_avail">
										<?php
										$avail_quiz_names = array();

										if(!empty($avail_quizzes))
											foreach ($avail_quizzes as $row)
											{
												if(isset($quiz_name)) {
													if ($row['quiz_name'] == $quiz_name) {
														echo '<option value="' . $row['quiz_name'] . '" selected>' . $row['quiz_name'] . '</option>';
													}
												}
												else{
													echo '<option value="' . $row['quiz_name'] . '">' . $row['quiz_name'] . '</option>';
												}
											}
										?>
									</select>
								</div>
                            </div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Question</label>
										<input type="text" class="form-control" id="que" name="que">
									</div>
								</div>
							</div>
							Add Answers Below
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 1</label>
										<input type="text" class="form-control" id="ans1" name="ans1">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check1">
										<input type="checkbox" id="check1" name="check1" class="mdl-checkbox__input" value="1">
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 2</label>
										<input type="text" class="form-control" id="ans2" name="ans2">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check2">
										<input type="checkbox" id="check2" name="check2" class="mdl-checkbox__input" value="1">
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 3</label>
										<input type="text" class="form-control" id="ans3" name="ans3">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check3">
										<input type="checkbox" id="check3" name="check3" class="mdl-checkbox__input" value="1">
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Answer 4</label>
										<input type="text" class="form-control" id="ans4" name="ans4">
									</div>
									<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="check4">
										<input type="checkbox" id="check4" name="check4" class="mdl-checkbox__input" value="1">
										<span class="mdl-checkbox__label">Is True?</span>
									</label>
								</div>
							</div>
							<a href="<?php echo base_url(); ?>index.php/home">
								<button type="button" class="btn btn-primary pull-left">Go Back</button>
							</a>
                            <button type="submit" class="btn btn-primary pull-right">Add More</button>
                            <div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
