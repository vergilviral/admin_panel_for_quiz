<?php

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','Your question has been successfully added.', 'success');};</script>";
}

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error . "', 'danger');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Edit Profile</h4>
						<p class="category">Edit your details to fill your user profile and profile picture.</p>
					</div>
					<div class="card-content">
						<form onsubmit="return confirm('Are you sure you want to update your profile with given details? These details can be changed later.');" action="<?php echo base_url(); ?>Edit_profile/edit_single_profile" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label" >Username</label>
										<input type="text" name="username" id="username" class="form-control" disabled="true" value="<?php echo $name; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Email address</label>
										<input type="email" name="email" id="email" class="form-control" value="<?php echo $user_details['user_email'] ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Fist Name</label>
										<input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo $user_details['first_name'] ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Last Name</label>
										<input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo $user_details['last_name'] ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Institute</label>
										<input type="text" name="institute_name" id="institute_name" class="form-control" disabled="true" value="<?php echo $user_details['institute_name'] ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Change Password</label>
										<input type="password" name="pass" id="pass" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Confirm Password</label>
										<input type="password" name="confirm_pass" id="confirm_pass" class="form-control" oninput="check(this)">
									</div>
								</div>
                                <script language='javascript' type='text/javascript'>
                                    function check(input) {
                                        if (input.value != document.getElementById('pass').value) {
                                            input.setCustomValidity('Passwords Do Not Match. Please Enter Same Password in Password and Confirm Password Field.');
                                        } else {
                                            input.setCustomValidity('');
                                        }
                                    }
                                </script>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Upload Profile Pic:<br /><br/>
                                    Current set image:<br/><br/>
                                    <img style="height: 200px;width: auto;" class="img-thumbnail" src="<?= base_url() ?>uploads/<?= $user_details['profile_pic_path'] ?>"><br/><br/>
                                    <input type="file" id="userfile" name="userfile"/><br/>
                                </div>
                            </div>
							<a href="<?php echo base_url(); ?>index.php/home">
								<button type="button" class="btn btn-primary pull-left">Go Back</button>
							</a>
                            <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                            <div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    window.onload =  function(){
        $("#userfile").fileinput({'showUpload':false});
    };
</script>
