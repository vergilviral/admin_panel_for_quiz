<?php

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','Your question has been successfully updated.', 'success');};</script>";

}
?>
<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}
?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Update Quiz</h4>
                        <p class="category">Edit details of the quiz below and click 'Done' to update the quiz / transfer the quiz.</p>
                    </div>
                    <div class="card-content">
                        <form onsubmit="return confirm('Are you sure you want to update quiz with given details? These details can be changed later from \'Edit Quiz\' option.');" action="<?php echo base_url(); ?>Edit_quiz/update_single_quiz" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Quiz Name</label>
                                        <input type="text" class="form-control" id="quiz" name="quiz" value="<?php echo $single_quiz_details[0]['quiz_name'] ?>" required oninvalid="setCustomValidity('Please Enter Quiz Name.')"
                                               onchange="try{setCustomValidity('')}catch(e){}">
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Subject Name</label>
                                        <input type="text" class="form-control" id="sub" name="sub" value="<?php echo $single_quiz_details[0]['subject_name'] ?>" required oninvalid="setCustomValidity('Please Enter Subject Name.')"
                                               onchange="try{setCustomValidity('')}catch(e){}">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label style="margin-right: 15px;">Quiz Type</label><br/>
                                            <select id="quiz_type" name="quiz_type" class="selectpicker" data-style="btn-info" required oninvalid="setCustomValidity('Oops. Something Seems Fishy. Please Go To Dashboard And Come Back.')"
                                                    onchange="checkType();">
                                                <option value="Normal" <?php if($single_quiz_details[0]['quiz_type'] == 'Normal'){echo 'Selected';} ?>>Normal</option>
                                                <option value="Traverse" <?php if($single_quiz_details[0]['quiz_type'] == 'Traverse'){echo 'Selected';} ?>>Traverse</option>
                                                <option value="FixedTime" <?php if($single_quiz_details[0]['quiz_type'] == 'FixedTime'){echo 'Selected';} ?>>Fixed Time</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" id="timingsfixed">
                                        <label style="padding-left: 15px; padding-top:10px;">Time Limit</label>
                                        <div class="col-md-12">
                                            <div class="col-md-4 timingsquiz">
                                                <input type="Number" class="form-control" id="hrs" name="hrs" oninvalid="setCustomValidity('Please Enter Proper Hours.')"
                                                       onchange="try{setCustomValidity('')}catch(e){}" value="<?php echo(floor($single_quiz_details[0]['time'] / 3600)); ?>"> Hrs
                                            </div>
                                            <div class="col-md-4 timingsquiz">
                                                <input type="Number" class="form-control" id="mins" name="mins" oninvalid="setCustomValidity('Please Enter Proper Minutes.')"
                                                       onchange="try{setCustomValidity('')}catch(e){}" value="<?php echo(($single_quiz_details[0]['time'] / 60) % 60); ?>"> Min
                                            </div>
                                            <div class="col-md-4 timingsquiz">
                                                <input type="Number" class="form-control" id="secs" name="secs" oninvalid="setCustomValidity('Please Enter Proper Seconds.')"
                                                       onchange="try{setCustomValidity('')}catch(e){}" value="<?php echo($single_quiz_details[0]['time'] % 60); ?>"> Secs
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Unique ID (You can't change this)</label>
                                        <input type="text" class="form-control" id="roomname" name="roomname" value="<?php echo $single_quiz_details[0]['roomname'] ?>" required oninvalid="setCustomValidity('Please Enter Quiz Name.')"
                                               onchange="try{setCustomValidity('')}catch(e){}" disabled>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="room" name="room" value="<?php echo $single_quiz_details[0]['roomname'] ?>" required oninvalid="setCustomValidity('Something went wrong.')"
                                   onchange="try{setCustomValidity('')}catch(e){}">
                            <a href="<?php echo base_url(); ?>index.php/home">
                                <button type="button" class="btn btn-primary pull-left">Go Back</button>
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">Done</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload =  function(){
        $("#userfile").fileinput({'showUpload':false});
        var option = document.getElementById('quiz_type');
        var otherblock = document.getElementById('timingsfixed');
        var selected = option.options[option.selectedIndex].text;

        if(selected == 'Fixed Time'){
            otherblock.style.display = ('block');
        }
        else{
            otherblock.style.display = ('none');
        }
    };
    function checkType(){
        var option = document.getElementById('quiz_type');
        var otherblock = document.getElementById('timingsfixed');
        var selected = option.options[option.selectedIndex].text;

        if(selected == 'Fixed Time'){
            otherblock.style.display = ('block');
        }
        else{
            otherblock.style.display = ('none');
        }
    }
</script>