<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Get Single Quiz Results</h4>
                        <p class="category">Click on 'Download' button to download quiz results in CSV file.</p>
                    </div>
                    <div class="card-content table-responsive">
                        <script>
                            function copyLink(id) {
                                var copyText = document.getElementById(id);
                                copyText.select();
                                document.execCommand("Copy");
                                alert("Copied the link: " + copyText.value);
                            }
                        </script>

                        <table class="table">
                            <tbody>
                            <?php
                            $avail_quiz_names = array();

                            if(!empty($avail_quizzes))
                                foreach ($avail_quizzes as $row)
                                {
                                    echo '<tr>';
                                    echo '<td>' . $row['quiz_name'] . '</td>';
                                    echo '<td></td>';
                                    echo '<td><form action="' . base_url() . 'Get_single_quiz_results/get_result" method="post"><input type="hidden" value="'.$row['roomname'].'" id="roomname" name="roomname"><button type="submit" class="btn btn-primary pull-right" id="'. $row['quiz_name'] .'"><i class="material-icons text-white" style="margin-right: 20px;">file_download</i>Download</button></form></td>';
                                    echo '</tr>';
                                }
                            ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url(); ?>index.php/home">
                                        <button type="button" class="btn btn-primary pull-left">Go Back</button>
                                    </a>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
