<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" data-background-color="purple">
						<h4 class="title">Available Questions</h4>
						<p class="category">Below are all questions you added in your selected quiz.</p>
					</div>
					<div class="card-content table-responsive">
						<table class="table">
							<tbody>
							<?php

								if(!empty($questions_list))
								foreach ($questions_list as $row)
								{
									echo '<tr>';
									echo '<td>' . $row['question'] . '</td>';
									echo '<td><form action="'. base_url() . 'Edit_questions/edit_single" method="post"><input type="hidden" name="selected_question" id="selected_question" value="'. $row['question'] .'"><button type="submit" class="btn btn-primary pull-right">Edit</button></form></td>';
									echo '</tr>';
								}


							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
