<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Re-order Questions</h4>
                        <p class="category">Hold and drag question to re-order.</p>
                    </div>
                    <div class="card-content table-responsive">
<!--                        <table class="table">-->
<!--                            <tbody>-->
<!--                            --><?php
//
//                            if(!empty($questions_list))
//                                foreach ($questions_list as $row)
//                                {
//                                    echo '<tr>';
//                                    echo '<td>' . $row['question'] . '</td>';
//                                    echo '<td><form action="'. base_url() . 'Edit_questions/edit_single" method="post"><input type="hidden" name="selected_question" id="selected_question" value="'. $row['question'] .'"><button type="submit" class="btn btn-primary pull-right">Edit</button></form></td>';
//                                    echo '</tr>';
//                                }
//
//
//                            ?>
<!--                            </tbody>-->
<!--                        </table>-->
                        <ul id="sortable">
                            <?php if(!empty($questions_list))
                            foreach ($questions_list as $row) {
                            echo '<li class="ui-state-default" id="'. $row['id'] .'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' . $row['question'] . '</li>';
                            }
                            ?>
                        </ul>
                        <a href="<?php echo base_url(); ?>index.php/home">
                            <button type="button" class="btn btn-primary pull-left">Go to Dashboard</button>
                        </a>
                        <form action="<?php echo base_url(); ?>Order_questions/update_order" method="post">
                            <input type="hidden" value="" id="mainorder" name="mainorder"/>
                        <button type="submit" class="btn btn-primary pull-right">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>