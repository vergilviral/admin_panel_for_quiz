<?php

if(isset($error)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $error ."', 'danger');};</script>";
}

if(isset($message)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $message ."', 'success');};</script>";
}

if(isset($msg)){
    echo "<script>window.onload = function() {demo.showNotification('top','center','". $msg ."', 'success');};</script>";
}

?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Delete Quiz</h4>
                        <p class="category">Select the quiz from list below and click on button below to delete the selected quiz.</p>
                    </div>
                    <div class="card-content">
                        <form onsubmit="return confirm('Do you really want to delete this quiz?');" action="<?php echo base_url(); ?>Delete_quiz/delete" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Select Quiz To Edit</p>
                                    <select id="quiz_names_avail" name="quiz_names_avail" class="selectpicker" data-style="btn-info" required oninvalid="setCustomValidity('Oops. Something went wrong. Please go to dashboard and come back to see your quizzes.')"
                                            onchange="try{setCustomValidity('')}catch(e){}">
                                        <?php
                                        $avail_quiz_names = array();

                                        if(!empty($avail_quizzes))
                                            foreach ($avail_quizzes as $row)
                                            {
                                                if(isset($quiz_name)) {
                                                    if ($row['quiz_name'] == $quiz_name) {
                                                        echo '<option value="' . $row['quiz_name'] . '" selected>' . $row['quiz_name'] . '</option>';
                                                    }
                                                }
                                                else{
                                                    echo '<option value="' . $row['quiz_name'] . '">' . $row['quiz_name'] . '</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <a href="<?php echo base_url(); ?>index.php/home">
                                <button type="button" class="btn btn-primary pull-left">Go Back</button>
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">Delete Selected Quiz</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
